module.exports = {
    maxWorkers: 10,
    testEnvironment: 'jsdom',
    moduleFileExtensions: ['js', 'json'],
    coverageReporters: ['clover', 'json', 'lcov', 'text', 'text-summary'],
    collectCoverageFrom: [
        'src/**/*.js', // Adjust the pattern to match your project structure
        '!dist',
        '!node_modules',
    ],
    coveragePathIgnorePatterns: ['/node_modules/'],
    coverageDirectory: '../../coverage',
    collectCoverage: true,
}
