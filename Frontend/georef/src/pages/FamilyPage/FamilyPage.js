/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
import React, { useEffect, useState } from 'react'
import FamilyPageComponent from '../../components/FamilyPageCompenent/FamilyPageComponent'

export async function getFamily(url, setFamily) {
    try {
        const getData = await fetch(url, {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
            },
        })
        if (getData) {
            const getDataJSON = await getData.json()
            setFamily(getDataJSON)
        }
    } catch {
        console.log("Pas de réponse de l'API")
    }
}

function FamilyPage() {

    const [family, setFamily] = useState([])

    const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllFamily`

    useEffect(() => {
        getFamily(url, setFamily)
    }, [])

    return (
        <div className="mt-[12.15rem] mb-12 container flex lg:felx-col w-[80rem] m-auto justify-center items-center flex-wrap family_design">
            {family.map((family) => (
                <FamilyPageComponent
                    family={family.label}
                    link={`/family/${family.label}`}
                    key={family.id}
                    picture={family.familyPicture}
                />
            ))}
        </div>
    )
}

export default FamilyPage
