/* eslint-disable react/function-component-definition */
/* eslint-disable react/button-has-type */
/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
import React, { useState, useContext, useCallback, useEffect } from 'react'
import AuthContext from '../../context/authContext'
import DisplayOneMineral from '../../components/DisplayMineral/DisplayOneMineral'

const AdminCheck = () => {
    const authContext = useContext(AuthContext)
    const [minerals, setMinerals] = useState([])
    const [id, setId] = useState()

    const url = `${process.env.REACT_APP_DB_CONNECT}/admin/getAllMineralsNotChecked`

    const getMinerals = useCallback(async () => {
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                    Authorization: `Bearer ${authContext.token}`,
                },
            })
            if (getData) {
                const getDataJSON = await getData.json()
                setMinerals(getDataJSON)
            }
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [minerals])

    useEffect(() => {
        getMinerals()
    }, [])

    const changeId = (e) => {
        if (e.target.selectedOptions[0].id !== id) {
            setId(e.target.selectedOptions[0].id)
            getMinerals()
        }
    }

    const checkClick = async (id) => {
        try {
            await fetch(`${process.env.REACT_APP_DB_CONNECT}/admin/check/${id}`, {
                method: 'PUT',
                headers: {
                    Accept: 'multipart/application/json',
                    Authorization: `Bearer ${authContext.token}`,
                },
            })
        } catch {
            console.log("Pas de réponse de l'API")
        }
        setId()
        getMinerals()
    }

    const valideClick = async (id) => {
        try {
            await fetch(`${process.env.REACT_APP_DB_CONNECT}/admin/display/${id}`, {
                method: 'PUT',
                headers: {
                    Accept: 'multipart/application/json',
                    Authorization: `Bearer ${authContext.token}`,
                },
            })
        } catch {
            console.log("Pas de réponse de l'API")
        }
        setId()
        getMinerals()
    }

    const deleteClick = async (id) => {
        try {
            await fetch(`${process.env.REACT_APP_DB_CONNECT}/admin/delete/${id}`, {
                method: 'DELETE',
                headers: {
                    Accept: 'multipart/application/json',
                    Authorization: `Bearer ${authContext.token}`,
                },
            })
        } catch {
            console.log("Pas de réponse de l'API")
        }
        setId()
        getMinerals()
    }

    if (minerals.length !== 0) {
        return (
            <div className="mt-[14rem] mb-[5rem]">
                <div className="bg-slate-600 rounded-2xl p-4 w-1/2 text-center m-auto admin_check">
                    <h2 className="uppercase text-2xl text-center text-white font-bold mb-2">
                        Check des minéraux
                    </h2>
                    <p className="text-white mb-2">
                        Minéraux restants à checker ({minerals.length}) :
                    </p>
                    <div className="">
                        <select
                            name="adminCheck"
                            className="bg-slate-200 mb-5 p-2 rounded-xl mt-2"
                            onChange={(e) => {
                                changeId(e)
                            }}
                        >
                            <option value="vide" />
                            {minerals.map((mineral) => (
                                <option
                                    value={mineral.name}
                                    id={mineral.id}
                                    key={mineral.id}
                                >
                                    {mineral.name}
                                </option>
                            ))}
                        </select>
                        {id && (
                            <div className="flex-initial">
                                <button
                                    className="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 border border-green-700 rounded-xl ml-5 flex-col "
                                    onClick={() => {
                                        valideClick(id)
                                    }}
                                >
                                    Valider
                                </button>

                                <button
                                    className="bg-orange-500 hover:bg-orange-700 text-white font-bold py-1 px-2 border border-orange-700 rounded-xl ml-5 flex-col"
                                    onClick={() => {
                                        checkClick(id)
                                    }}
                                >
                                    CHECK
                                </button>

                                <button
                                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-700 rounded-xl ml-5 flex-col"
                                    onClick={() => {
                                        deleteClick(id)
                                    }}
                                >
                                    Supprimer
                                </button>
                            </div>
                        )}
                    </div>
                </div>
                <div className="mt-8 p-5 display_mineral_admin">
                    {id && <DisplayOneMineral id={id} />}
                </div>
            </div>
        )
    }
    if (minerals.length === 0) {
        return (
            <div className="mt-[11rem]">
                <h2 className="uppercase text-2xl m-auto text-center mb-6">
                    Aucun Minéral à Check!
                </h2>
            </div>
        )
    }
}

export default AdminCheck
