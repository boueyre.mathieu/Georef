/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
import React, { useState, useContext } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import AuthContext from '../../context/authContext'
import GetCrystalMesh from '../../components/AddMineral/GetCrystalMesh'
import GetFamily from '../../components/AddMineral/GetFamily'
import GetHabitus from '../../components/AddMineral/GetHabitus'

function AddMineral() {
    const { register, handleSubmit, resetField } = useForm()
    const [picture, setPicture] = useState()

    const [opacity, setOpacity] = useState()
    const [glow, setGlow] = useState()

    const navigate = useNavigate()

    const authContext = useContext(AuthContext)

    const [crystalMesh, setCrystalMesh] = useState()
    const crystalMeshValue = (value) => {
        setCrystalMesh(parseInt(value))
    }

    const [family, setFamily] = useState()
    const familyValue = (value) => {
        setFamily(parseInt(value))
    }

    const [habitus, setHabitus] = useState()
    const habitusValue = (value) => {
        setHabitus(parseInt(value))
    }

    const onSubmit = (data) => {
        console.log(data.picture[0])

        const { name } = data
        const picture = data.picture[0]
        const { glow } = data
        const { color } = data
        const { density } = data
        const { hardness } = data
        const { description } = data
        const { chimicalFormula } = data
        const { opacity } = data

        const formData = new FormData()

        formData.append('name', name)
        formData.append('crystalMesh', crystalMesh)
        formData.append('color', color)
        formData.append('chemicalFormula', chimicalFormula)
        formData.append('description', description)
        formData.append('family', family)
        formData.append('glow', glow)
        formData.append('opacity', opacity)
        formData.append('density', density)
        formData.append('hardness', hardness)
        formData.append('image', picture)
        formData.append('habitus', habitus)

        const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/createMineral`

        const handleForm = async () => {
            try {
                const addmineral = await fetch(url, {
                    method: 'POST',
                    body: formData,
                    headers: {
                        Accept: 'multipart/form-data',
                        Authorization: `Bearer ${authContext.token}`,
                    },
                })

                const addMineralResult = await addmineral.json()
                console.log(addMineralResult)

                resetField('name')
                resetField('mineralFamily')
                resetField('description')
                resetField('chimicalFomula')
                resetField('crystalMesh')
                resetField('hardness')
                resetField('color')
                resetField('glow')
                resetField('opacity')
                resetField('density')
                resetField('habitus')
                resetField('picture')

                navigate('/information')
            } catch {
                console.log("Pas de réponse de l'API")
            }
        }
        handleForm()
    }

    function displayPicture(picture) {
        const pictureUplaod = URL.createObjectURL(picture)
        setPicture(pictureUplaod)
    }

    return (
        <section className="mt-[15rem] max-w-[1600px] w-full m-auto">
            <div className="w-full">
                <div className="bg-white">
                    <h2 className="uppercase text-3xl m-auto text-center font-bold">
                        ajouter un minéral
                    </h2>
                    <form
                        onSubmit={handleSubmit(onSubmit)}
                        className="flex flex-col text-center w-full justify-center items-center m-auto mt-12"
                    >
                        <div className="flex field_component">
                            <div className="flex-col text-justify mr-[5rem] form_design_addMineral">
                                <div className="mb-2 field_addMineral">
                                    <label htmlFor="name" id="name">
                                        Nom du minéral
                                    </label>
                                    <input
                                        type="text"
                                        id="name"
                                        name="name"
                                        className="bg-slate-200 rounded-md ml-[3rem] p-[0.30rem] pl-[0.5rem] input_addMineral w-[16rem]"
                                        {...register('name', {
                                            required:
                                                'Le nom du miénral est manquant',
                                        })}
                                    />
                                </div>
                                <div className="flex mb-2 field_addMineral">
                                    <label
                                        htmlFor="familyMineral"
                                        id="familyMineral"
                                    >
                                        Famille du minéral
                                    </label>
                                    <GetFamily getValue={familyValue} />
                                </div>
                                <div className="flex mb-2 field_addMineral">
                                    <label
                                        htmlFor="crystalmesh"
                                        id="crystalMesh"
                                    >
                                        Maille cristaline
                                    </label>
                                    <GetCrystalMesh
                                        getValue={crystalMeshValue}
                                    />
                                </div>
                                <div className="mb-2 field_addMineral">
                                    <label
                                        htmlFor="chimicalFormula"
                                        id="chimicalFormula"
                                    >
                                        Formule chimique
                                    </label>
                                    <input
                                        type="text"
                                        name="chimicalFormula"
                                        {...register('chimicalFormula', {
                                            required:
                                                'Formule chimique requise',
                                        })}
                                        className="bg-slate-200 ml-[2.3rem] rounded-md input_addMineral p-[0.30rem] pl-[0.5rem] w-[16rem]"
                                    />
                                </div>
                                <div className="flex mb-2 field_addMineral">
                                    <label htmlFor="habitus" id="habitus">
                                        Habitus du minéral
                                    </label>
                                    <GetHabitus getValue={habitusValue} />
                                </div>
                                <div className="mb-2 field_addMineral">
                                    <label htmlFor="color" id="color">
                                        Couleur du minéral
                                    </label>
                                    <input
                                        type="text"
                                        name="color"
                                        id="color"
                                        {...register('color', {
                                            required:
                                                'la couleur du minéral est obligatoire',
                                        })}
                                        className="bg-slate-200 rounded-md ml-[1.8rem] input_addMineral p-[0.30rem] pl-[0.5rem] w-[16rem]"
                                    />
                                </div>
                                <div className="mb-2 field_addMineral">
                                    <label htmlFor="opacity" id="opacity">
                                        Opacité du minéral
                                    </label>
                                    <select
                                        name="opacity"
                                        id="opacity"
                                        value={opacity}
                                        onChange={(e) => {
                                            setOpacity(e.target.value)
                                        }}
                                        {...register('opacity', {
                                            required:
                                                "L'opacité est obligatoire",
                                        })}
                                        className="bg-slate-200 rounded-md ml-[1.85rem] input_addMineral p-[0.30rem] pl-[0.5rem] w-[16rem]"
                                    >
                                        <option value="vide" />
                                        <option value="transparent">
                                            Transparent
                                        </option>
                                        <option value="semi-transparent">
                                            Semi-transparent
                                        </option>
                                        <option value="translucide">
                                            Translucide
                                        </option>
                                        <option value="non-transparent">
                                            Non-transparent
                                        </option>
                                        <option value="opaque">Opaque</option>
                                    </select>
                                </div>
                                <div className="mb-2 field_addMineral">
                                    <label htmlFor="glow" id="glow">
                                        Eclat
                                    </label>
                                    <select
                                        name="glow"
                                        id="glow"
                                        value={glow}
                                        onChange={(e) =>
                                            setGlow(e.target.value)
                                        }
                                        {...register('glow', {
                                            required:
                                                "L'éclat du minéral est obligatoire",
                                        })}
                                        className="bg-slate-200 rounded-md ml-[8.25rem] input_addMineral p-[0.30rem] pl-[0.5rem] w-[16rem]"
                                    >
                                        <option value="vide" />
                                        <option value="adamantin">
                                            Adamantin
                                        </option>
                                        <option value="vitreux">Vitreux</option>
                                        <option value="resineux">
                                            Résineux
                                        </option>
                                        <option value="nacre">Nacré</option>
                                        <option value="soyeux">Soyeux</option>
                                        <option value="gras">Gras</option>
                                        <option value="terreux">Terreux</option>
                                    </select>
                                </div>
                                <div className="mb-2 field_addMineral">
                                    <label htmlFor="hardness" id="hardness">
                                        Dureté du minéral
                                    </label>
                                    <input
                                        type="number"
                                        id="number"
                                        name="number"
                                        min="0.5"
                                        max="20"
                                        step="0.05"
                                        {...register('hardness', {
                                            require:
                                                'La dureté du minéral est obligatoire',
                                        })}
                                        className="bg-slate-200 rounded-md ml-[2.25rem] input_addMineral p-[0.30rem] pl-[0.5rem] w-[16.1rem]"
                                    />
                                </div>
                                <div className="mb-2 field_addMineral">
                                    <label htmlFor="density" id="density">
                                        Densité du minéral
                                    </label>
                                    <input
                                        type="number"
                                        id="number"
                                        name="number"
                                        min="0.5"
                                        max="20"
                                        step="0.05"
                                        {...register('density', {
                                            require:
                                                'La densité du minéral est obligatoire',
                                        })}
                                        className="bg-slate-200 rounded-md ml-[1.85rem] input_addMineral p-[0.30rem] pl-[0.8rem] w-[16.1rem]"
                                    />
                                </div>
                            </div>
                            <div className="flex flex-col justify-start items-start text-left xl:w-[400px] img_input_addMineral">
                                <label htmlFor="picture">
                                    Image du minéral
                                </label>
                                <input
                                    type="file"
                                    id="picture"
                                    name="picture"
                                    {...register('picture', {
                                        required: 'Le champ image est manquant',
                                    })}
                                    onChange={(e) =>
                                        displayPicture(e.target.files[0])
                                    }
                                />
                                {picture && (
                                    <img
                                        src={picture}
                                        alt="Image Téléchargée"
                                        className="w-[400px]"
                                    />
                                )}
                            </div>
                        </div>
                        <div className="flex flex-col w-full m-auto items-center">
                            <label
                                htmlFor="description"
                                className="mb-3 text-left mt-8"
                            >
                                Description du minéral
                            </label>
                            <textarea
                                name="description"
                                id="description"
                                cols="30"
                                rows="10"
                                {...register('description', {
                                    required:
                                        'La description du minérale est obligatoire',
                                })}
                                className="bg-slate-200 rounded-3xl w-[80%] mb-8 description_add_mineral p-6"
                            />
                        </div>
                        <div>
                            <button
                                type="submit"
                                className="bg-slate-500 p-3 rounded-xl text-white font-bold text-xl mb-[2rem]"
                            >
                                Soumettre le minéral
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    )
}

export default AddMineral
