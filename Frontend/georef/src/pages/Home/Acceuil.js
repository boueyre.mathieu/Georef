/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable import/extensions */
/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import { Link } from 'react-router-dom'
import pictureAccueil from '../../public/832ce5b5-c474-4978-af83-7f128c82f25e-sansfond.png'
import pictureMineral from '../../public/istockphoto-119387130-612x612.jpg'
import familyPagePicture from '../../public/imgFamilyPage.png'
import mineralPagePicture from '../../public/mineralPage.png'

function Acceuil() {
    return (
        <main className="h-full w-full main_acceuil pb-12 m-auto min-h-full mt-[15rem] flex flex-col items-center justify-center">
            <h1 className="upercase text-6xl font-bold mb-[-4rem]" id="title">
                GEOREF
            </h1>

            <section className="mb-[5rem] img_section m-auto flex">
                <img
                    src={pictureAccueil}
                    alt="Logo présentation"
                    className=" img_acceuil_design block h-auto w-[600px] m-auto"
                />
            </section>
            {/* <section className=" description_accueil_design h-[14rem] bg-gray-600 text-xl font-bold flex items-center text-white text-center box-border m-auto w-[60%] p-5 rounded-xl">
                <p>
                    Ce site aura pour objectifs de représenter et d'identifier
                    la plupart des minéraux qu'il est susceptible de rencontrer
                    dans la nature. Pour chaque minéral une fiche de
                    présentation vous sera fournie avec toute leur propriétée.{' '}
                    <br />
                    Libre à vous de rajouter un minéral afin d'établir, afin
                    d'établir un référencement le plus complet.
                </p>
            </section> */}

            <section className="mt-[5rem] w-full m-auto flex justify-center">
                <div className="flex bg-slate-300 w-[70%] acceuil_description_website">
                    <img
                        src={familyPagePicture}
                        alt="présentation de la page family"
                        className="w-[650px] img_description_website"
                    />
                    <div className="text-center flex items-center justify-center w-full description_desciption_website">
                        <p className="text-xl text-center flex flex-col justify-center text-size">
                            Trouvez facilement les minéraux présent dans une
                            famille en parcourant la page
                            <Link to="/familyMineral" className="font-bold">
                                Famille Minéralogique
                            </Link>
                        </p>
                    </div>
                </div>
            </section>

            <img src={pictureMineral} alt="image minérale" />

            <section className="mt-[5rem] w-full mb-12 flex mauto justify-center">
                <div className="flex bg-slate-300 w-[70%] acceuil_description_website">
                    <div className="text-center flex items-center justify-center w-full description_desciption_website">
                        <p className="font-bold text-xl text-center flex flex-col justify-center">
                            Chaque minérale aurra sa propre page descriptive,
                            avec toute les caractéristiques qui lui sont
                            propres.
                        </p>
                    </div>
                    <img
                        src={mineralPagePicture}
                        alt="présentation de la page family"
                        className="w-[650px] img_description_website"
                    />
                </div>
            </section>
        </main>
    )
}

export default Acceuil
