/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { BrowserRouter as Router } from 'react-router-dom'
import Acceuil from './Acceuil'

describe('Verify page Home', () => {
    test('Home rendering without crash', () => {
        render(
            <Router>
                <Acceuil />
            </Router>,
        )
    })
})
