/* eslint-disable react/button-has-type */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
import React, { useState, useContext } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import authContext from '../../context/authContext'
import InformationGet from '../../components/Information/InformationGet'
import GetMineralsUser from '../../components/Information/MineralsUser'

function Information() {
    const [userData, setUserData] = useState()
    const [displayModal, setDisplayModal] = useState(false)
    const { register, handleSubmit } = useForm()
    const [changePwdModal, setChangePwdModal] = useState(false)
    const [mineralDisplay, setMineralDisplay] = useState([])
    const [idMineral, setIdMineral] = useState()

    const redirect = useNavigate()

    const authCtx = useContext(authContext)

    const handleForm = (data, e) => {
        e.preventDefault()

        const { oldPassword } = data
        const { newPassword } = data
        const { verifyNewPassword } = data

        const url = `${process.env.REACT_APP_DB_CONNECT}/auth/changePwd`

        const fetchHandleForm = async () => {
            try {
                const resultPassword = await fetch(url, {
                    method: 'PUT',
                    body: JSON.stringify({
                        oldPassword,
                        newPassword,
                        verifyNewPassword,
                    }),
                    headers: {
                        'content-type': 'application/json',
                        Authorization: `Bearer ${authCtx.token}`,
                    },
                })
                const dataResultPassword = await resultPassword.json()
                if (dataResultPassword) {
                    setChangePwdModal(true)
                }
            } catch {
                console.log('Pas de réponse de l api')
            }
        }
        fetchHandleForm()
    }

    const displayModalFunction = () => {
        setDisplayModal((displayModal) => !displayModal)
    }

    const disconnexionButton = () => {
        authCtx.logout()
        redirect('/auth')
    }

    return (
        <div className="mb-[5rem]">
            <div>
                <div className="mt-[15rem] shadow shadow-slate-500/50 p-2 w-1/2 m-auto rounded-3xl identification_user">
                    <div>
                        <h2 className="uppercase m-auto text-center mt-12 mb-5 font-bold">
                            Vos informations
                        </h2>
                        <div className="">
                            <div>
                                <InformationGet />
                            </div>

                            <div>
                                {!displayModal && (
                                    <button
                                        onClick={() => displayModalFunction()}
                                        className="border p-4 bg-slate-500 text-white font-bold m-auto flex justify-center mb-[2rem] rounded-xl"
                                    >
                                        Changer votre mot de passe
                                    </button>
                                )}
                                {displayModal && (
                                    <form
                                        onSubmit={handleSubmit(handleForm)}
                                        className="flex flex-col text-center w-[20rem] m-auto left-1/2"
                                    >
                                        <label
                                            htmlFor="oldPassword"
                                            className="mb-2 font-bold"
                                        >
                                            Ancien mot de passe:
                                        </label>
                                        <input
                                            type="password"
                                            id="oldPassword"
                                            name="oldPassword"
                                            className="bg-slate-200 mb-2 p-2 rounded-xl"
                                            {...register('oldPassword', {
                                                required: 'Champ Obligatoire',
                                            })}
                                        />
                                        <label
                                            htmlFor="nawPassword"
                                            className="mb-2 font-bold"
                                        >
                                            Nouveau mot de passe:
                                        </label>
                                        <input
                                            type="password"
                                            id="newPassword"
                                            name="newPassword"
                                            className="bg-slate-200 mb-2 p-2 rounded-xl"
                                            {...register('newPassword', {
                                                required: 'Champ obligatoire',
                                            })}
                                        />
                                        <label
                                            htmlFor="verifyPassword"
                                            className="mb-2 font-bold"
                                        >
                                            Vérification du nouveau mot de
                                            passe:
                                        </label>
                                        <input
                                            type="password"
                                            id="verifyPassword"
                                            name="verifyNewPassword"
                                            className="bg-slate-200 p-2 rounded-xl"
                                            {...register('verifyNewPassword', {
                                                required: 'Champ Obligatoire',
                                            })}
                                        />
                                        <button
                                            type="submit"
                                            className="p-3 bg-slate-500 text-white mt-5 rounded-xl font-bold mb-5"
                                        >
                                            Soumettre
                                        </button>
                                    </form>
                                )}
                                {changePwdModal && (
                                    <div className="w-full h-full bg-slate-400/80 flex justify-center items-center text-center m-auto fixed top-0 left-0">
                                        <div className="font-bold text-2xl w-1/3 bg-white p-12 rounded-3xl">
                                            <p>
                                                Le changement de votre mot de
                                                passe a bien été pris en compte.
                                                Merci de vous reconnecter avec
                                                le nouveau mot de passe.
                                            </p>
                                            <button
                                                onClick={disconnexionButton}
                                                className="p-5 mt-8 bg-slate-600 rounded-3xl text-white text-xl"
                                            >
                                                Accepter
                                            </button>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-12 w-[70%] m-auto getMineral_modify">
                    <div className="font-bold text-center mb-12">
                        <h3 className="pt-8">
                            Vos informations sur les minéraux que vous avez
                            ajouté(s)
                        </h3>
                    </div>
                    <div className="pb-8 mb-8 space-y-4">
                        <GetMineralsUser />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Information
