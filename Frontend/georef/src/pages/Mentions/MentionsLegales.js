/* eslint-disable react/no-unescaped-entities */
import React from 'react'

function MentionsLegales() {
    return (
        <div className="mt-[15rem] m-auto flex flex-col justify-center items-center">
            <h2 className="font-bold uppercase m-auto mb-12">
                Politque de confidentialité
            </h2>
            <h3 className="font-bold mb-8">
                Dernière mise à jours: 22/09/2023
            </h3>
            <div className="text-justify md:w-[70%] mb-12">
                <p>
                    Bienvenue sur Georef. Chez Georef, nous prenons la
                    protection de vos informations personnelles très au sérieux.
                    Cette politique de confidentialité explique comment nous
                    collectons, utilisons, partageons et protégeons vos
                    informations lorsque vous utilisez notre site web. En
                    utilisant le Site, vous consentez aux pratiques décrites
                    dans cette politique de confidentialité.
                </p>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-5">
                    Collecte et Utilisation des informations
                </h3>
                <p>
                    Nous collectons certaines informations personnelles lorsque
                    vous visitez notre Site, notamment lorsque vous vous
                    inscrivez, remplissez un formulaire ou interagissez avec nos
                    services. Les informations que nous pouvons collecter
                    incluent, sans s'y limiter :
                </p>

                <ul className="list-decimal ml-12 mt-2 mb-2">
                    <li>
                        Informations d'identification personnelles telles que
                        votre nom, votre adresse e-mail.
                    </li>
                    <li>
                        Informations de connexion, y compris les mots de passe,
                        qui sont stockés sous une forme cryptée pour garantir
                        leur sécurité.
                    </li>
                </ul>

                <p>
                    Nous utilisons ces informations pour les finalités suivante:
                </p>

                <ul className="list-disc ml-12 mt-2 mb-2">
                    <li>
                        Pour vous fournir les produits, services et informations
                        demandés.
                    </li>
                    <li>
                        Pour améliorer et personnaliser votre expérience sur
                        notre Site.
                    </li>
                    <li>
                        Pour communiquer avec vous et répondre à vos questions.
                    </li>
                    <li>Pour vous informer des mises à jour.</li>
                    <li>
                        Pour garantir la sécurité de vos informations
                        personnelles.
                    </li>
                </ul>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-5">
                    Images Envoyées par les Utiliteurs
                </h3>
                <p className="mb-5">
                    Lorsque vous utilisez notre Site, vous pouvez avoir la
                    possibilité de télécharger, partager ou envoyer des images
                    ou des fichiers visuels. Nous vous rappelons que toute image
                    ou fichier que vous partagez doit être libre de droits ou
                    vous devez détenir les droits de propriété intellectuelle
                    nécessaires pour les partager. Nous n'encourageons ni ne
                    tolérons la violation des droits d'auteur ou d'autres droits
                    de propriété intellectuelle.
                </p>
                <p className="mb-5">
                    Nous nous réservons le droit de supprimer tout contenu
                    visuel qui enfreint les droits d'auteur, les marques
                    déposées, les droits de confidentialité ou tout autre droit
                    de propriété intellectuelle d'un tiers, conformément à la
                    législation applicable. Nous ne sommes pas responsables de
                    la vérification de la légalité des images ou fichiers
                    visuels que vous partagez sur notre Site, mais nous prenons
                    au sérieux toute plainte ou demande de retrait liée à une
                    violation présumée des droits de propriété intellectuelle.
                </p>
                <p>
                    En partageant des images ou des fichiers visuels sur notre
                    Site, vous consentez à ce que nous les utilisions dans le
                    cadre des finalités prévues par cette politique de
                    confidentialité, notamment pour vous fournir des services et
                    améliorer votre expérience sur notre Site.
                </p>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-2">Protections des informations</h3>
                <p>
                    Nous utilisons des mesures de sécurité techniques,
                    administratives et physiques appropriées pour protéger vos
                    informations personnelles contre toute perte, tout accès non
                    autorisé, toute divulgation, toute altération et toute
                    destruction. Vos mots de passe sont cryptés et stockés en
                    toute sécurité.
                </p>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-2">Partage d'information</h3>
                <p>
                    Nous ne partageons pas vos informations personnelles avec
                    des tiers, sauf dans les cas suivants :
                </p>

                <ul className="list-disc ml-12 mt-2 mb-2">
                    <li>Avec votre consentement explicite.</li>
                    <li>
                        Lorsque cela est nécessaire pour vous fournir les
                        produits ou services que vous avez demandés.
                    </li>
                    <li>Pour se conformer à des obligations légales.</li>
                    <li>
                        Dans le cadre de fusions, acquisitions ou autres
                        transactions similaires.
                    </li>
                </ul>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-2">
                    Durée de Conservation des Données
                </h3>
                <p>
                    Nous conserverons vos informations personnelles aussi
                    longtemps que nécessaire pour les finalités pour lesquelles
                    elles ont été collectées, ou selon ce qui est requis par la
                    loi.
                </p>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-2">Vos Droits</h3>
                <p>
                    Vous avez le droit de demander l'accès à vos informations
                    personnelles, de les corriger, de les supprimer ou de vous
                    opposer à leur traitement. Pour exercer ces droits, veuillez
                    nous contacter à l'adresse georefExpo@gmail.com.
                </p>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-2">
                    Logo Réalisé par une Intelligence Artificielle (IA)
                </h3>
                <p>
                    Nous tenons à informer nos utilisateurs que le logo de notre
                    site web a été créé par une Intelligence Artificielle (IA)
                    avancée. Aucune information personnelle ou donnée sensible
                    de nos utilisateurs n'a été utilisée dans le processus de
                    création du logo par cette IA. L'IA a été programmée pour
                    générer un design unique et original, en utilisant des
                    paramètres et des directives spécifiques, sans accéder à des
                    informations confidentielles ou personnelles. L'utilisation
                    de l'IA pour la création de notre logo garantit que le
                    processus de conception est automatisé et exempt de tout
                    biais personnel. Nous prenons au sérieux la protection de
                    vos informations et nous veillons à ce que la création du
                    logo soit conforme à notre politique de confidentialité et à
                    toutes les lois applicables en matière de protection des
                    données. Si vous avez des questions ou des préoccupations
                    concernant le processus de création du logo par l'IA,
                    n'hésitez pas à nous contacter à l'adresse
                    georefExpo@gmail.com.
                </p>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-2">
                    Mises à Jour de la Politique de Confidentialité
                </h3>
                <p>
                    Nous pouvons mettre à jour cette politique de
                    confidentialité de temps à autre pour refléter les
                    changements dans nos pratiques ou pour d'autres raisons. La
                    date de la dernière mise à jour sera indiquée en haut de
                    cette page. Nous vous encourageons à consulter régulièrement
                    cette page pour rester informé(e) de nos pratiques en
                    matière de confidentialité.
                </p>
            </div>

            <div className="text-justify md:w-[70%] mb-12">
                <h3 className="font-bold mb-2">Contactez-nous</h3>
                <p>
                    Si vous avez des questions ou des préoccupations concernant
                    notre politique de confidentialité, veuillez nous contacter
                    à l'adresse suivante : georefExpo@gmail.com.
                </p>
            </div>
        </div>
    )
}

export default MentionsLegales
