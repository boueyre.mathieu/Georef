/* eslint-disable prettier/prettier */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-use-before-define */
/* eslint-disable eqeqeq */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable no-unused-expressions */
import React, { useState, useCallback, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'

function Research() {
    const [minerals, setMinerals] = useState([])
    const [families, setFamilies] = useState([])
    const [habituss, setHabituss] = useState([])
    const [crystalMeshs, setcrystalMeshs] = useState([])
    const [name, setName] = useState()
    const [family, setFamily] = useState()
    const [habitus, setHabitus] = useState()
    const [crystalMesh, setCrystalMesh] = useState()
    let mineralUrl
    let { nameLinked } = useParams()
    const [nameReceived, setNameReceived] = useState(nameLinked)

    history.pushState(null, null, '/research')

    const familyUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllFamily`
    const habitusUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllHabitus`
    const crystalMeshUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllcrystalMesh`

    if (!name) {
        mineralUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllMineralsToDisplay/`
    } else {
        mineralUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllMineralsToDisplay/${name}`
    }

    const getMinerals = async () => {
        try {
            const getData = await fetch(mineralUrl, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                },
            })
            if (getData) {
                let getDataJSON = await getData.json()
                if (family && family !== '') {
                    getDataJSON = getDataJSON.filter(
                        (mineral) => mineral.family_id == family,
                    )
                }
                if (habitus && habitus !== '') {
                    getDataJSON = getDataJSON.filter(
                        (mineral) => mineral.habitus_id == habitus,
                    )
                }
                if (crystalMesh && crystalMesh !== '') {
                    getDataJSON = getDataJSON.filter(
                        (mineral) => mineral.crystalMesh_id == crystalMesh,
                    )
                }
                setMinerals(getDataJSON)
            }
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }

    const getFamilies = useCallback(async () => {
        try {
            const getData = await fetch(familyUrl, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                },
            })
            if (getData) {
                const getDataJSON = await getData.json()
                setFamilies(getDataJSON)
            }
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [families])

    const getHabituss = useCallback(async () => {
        try {
            const getData = await fetch(habitusUrl, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                },
            })
            if (getData) {
                const getDataJSON = await getData.json()
                setHabituss(getDataJSON)
            }
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [habituss])

    const getCrystalMeshs = useCallback(async () => {
        try {
            const getData = await fetch(crystalMeshUrl, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                },
            })
            if (getData) {
                const getDataJSON = await getData.json()
                setcrystalMeshs(getDataJSON)
            }
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [crystalMeshs])

    useEffect(() => {
        getFamilies()
        getHabituss()
        getCrystalMeshs()
        submitForm()
    }, [])

    function linkMineral(mineral) {
        const family = families.find(
            (family) => mineral.family_id === family.id,
        )
        return `/family/${family.label}/${mineral.id}`
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        getMinerals()
    }

    const submitForm = () => {
        if (nameReceived !== null && typeof nameReceived !== 'undefined') {
            setName(nameReceived)
            handleSubmit({ preventDefault: () => {} })
            mineralUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllMineralsToDisplay/${nameReceived}`
            setNameReceived(null)
            nameLinked = null
        }
    }

    return (
        <div className="mt-[15rem]">
            <div className=" rounded-xl p-4 shadow shadow-slate-500/50 w-[80%] m-auto search_field">
                <h2 className="uppercase text-3xl text-center font-bold">
                    Rechercher votre minéral
                </h2>
                <div className="mt-4 flex">
                    <form onSubmit={handleSubmit} className="w-full">
                        <div className="flex flex-col mb-6">
                            <label
                                htmlFor="selectFamily"
                                className=" text-xl mb-2"
                            >
                                Sélection de la famille minéralogique:{' '}
                            </label>
                            <select
                                name="selectFamily"
                                className="bg-slate-200 p-2 rounded-md"
                                onChange={(e) => {
                                    setFamily(e.target.selectedOptions[0].id)
                                }}
                            >
                                <option value="vide">Aucune</option>
                                {families.map((family) => (
                                    <option
                                        value={family.label}
                                        id={family.id}
                                        key={family.id}
                                    >
                                        {family.label}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="flex flex-col mb-6">
                            <label
                                htmlFor="selectHabitus"
                                className="text-xl mb-2"
                            >
                                Sélection de l'habitus du minéral:
                            </label>
                            <select
                                name="selectHabitus"
                                className="bg-slate-200 p-2 rounded-md"
                                onChange={(e) => {
                                    setHabitus(e.target.selectedOptions[0].id)
                                }}
                            >
                                <option value="vide">Aucun</option>
                                {habituss.map((habitus) => (
                                    <option
                                        value={habitus.label}
                                        id={habitus.id}
                                        key={habitus.id}
                                    >
                                        {habitus.label}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="flex flex-col mb-6">
                            <label
                                htmlFor="selectCrystalMesh"
                                className="text-xl mb-2 "
                            >
                                Sélection de la maille cristalline
                            </label>
                            <select
                                name="selectCrystalMesh"
                                className="bg-slate-200 p-2 rounded-md"
                                onChange={(e) => {
                                    setCrystalMesh(
                                        e.target.selectedOptions[0].id,
                                    )
                                }}
                            >
                                <option value="vide">Aucune</option>
                                {crystalMeshs.map((crystalMesh) => (
                                    <option
                                        value={crystalMesh.label}
                                        id={crystalMesh.id}
                                        key={crystalMesh.id}
                                    >
                                        {crystalMesh.label}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="flex flex-col mb-8">
                            <label
                                htmlFor="searchField"
                                className="text-xl mb-2"
                            >
                                Recherchez directement votre minéral
                            </label>
                            <input
                                type="text"
                                name="recherche"
                                id="reherche"
                                value={name}
                                onChange={(e) => {
                                    setName(e.target.value)
                                }}
                                placeholder="Nom de votre minéral recherché"
                                className="bg-slate-200 p-2 rounded-md"
                            />
                        </div>
                        <button
                            className="bg-slate-400 p-3 rounded-xl text-white font-bold text-xl mb-2 self-end"
                            type="submit"
                        >
                            Rechercher
                        </button>
                    </form>
                </div>
            </div>
            {minerals && (
                <div className="mt-4 flex m-auto search_response justify-around container_card max-w-[1400px] w-full">
                    {minerals.map((mineral) => (
                        <Link
                            to={linkMineral(mineral)}
                            className="box-border w-[350px] h-[300px] link_card_search m-3"
                            key={mineral.id}
                        >
                            <div className="w-full h-full rounded-3xl overflow-hidden shadow-lg bg-slate-100 box-border shadow-slate-500/50 search_response_card">
                                <div className="w-[full] h-[250px] search_card_optimization">
                                    <img
                                        className="w-full h-full search_card_optimization_img object-fill"
                                        src={mineral.picturePath}
                                        alt="image du minéral"
                                    />
                                </div>
                                <div className="flex justify-center p-3">
                                    <p className="font-bold">{mineral.name}</p>
                                </div>
                            </div>
                        </Link>
                    ))}
                </div>
            )}
        </div>
    )
}
export default Research
