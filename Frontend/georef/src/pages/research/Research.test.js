/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { fireEvent, render } from '@testing-library/react'
import { describe, expect, jest } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import Research from './Research'

const displayMockData = [
    {
        id: 7,
        name: 'A moi',
        chemicalFormula: 'Super ma formule',
        hardness: '1.20',
        glow: 'gras',
        color: 'orange',
        opacity: '0.00',
        density: '12.50',
        picturePath:
            'http://localhost:4000/images/Gifs-from-Gifsoup-com-finding-nemo-16297856-320-180.gif1692188483429.undefined',
        description: '',
        createdAt: '2023-08-16T12:21:23.000Z',
        family_id: 7,
        habitus_id: 11,
        crystalMesh_id: 5,
        user_id: 4,
        isDisplay: true,
        isChecked: true,
    },
]

let windowFetchSpy

beforeEach(() => {
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch)
})

afterEach(() => {
    jest.resetAllMocks()
})

export default async function mockFetch(url) {
    if (
        url === `${process.env.REACT_APP_DB_CONNECT}/api/mineral/userMinerals`
    ) {
        return {
            ok: true,
            status: 200,
            json: async () => displayMockData,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

describe('verify research Page', () => {
    test('should rendering without crash', () => {
        render(
            <Router>
                <Research />
            </Router>,
        )
    })
})
