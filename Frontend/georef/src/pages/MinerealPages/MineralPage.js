/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import MineralList from '../../components/MineralPagesComponents/MineralList'
import FamilyList from '../../components/MineralPagesComponents/FamilyList'



function MineralPage() {
    const { family } = useParams()
    const { mineralId } = useParams()
    const [minerals, setMinerals] = useState([])
    const [urlParams, setUrlParams] = useState(family)

    const getAllMineralOneFamily = async () => {
        console.log(family)
        const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/family/${family}`
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application.json',
                },
            })

            const getDataJSON = await getData.json()
            setMinerals(getDataJSON)
        } catch {
            return "Pas de réponse de l'API"
        }
    }

    useEffect(() => {
        getAllMineralOneFamily()
    }, [])

    const onRefresh = () => {
        getAllMineralOneFamily()
    }

    if (family !== urlParams) {
        setUrlParams(family)
        onRefresh()
    }

    return (
        <section className="h-[1400px] height_configure flex justify-center block_section">
            <div className="mt-[12rem] mb-12 list_organization">
                <div className="flex flex-col">
                    <FamilyList familyParams={family} />
                    <MineralList
                        mineralsList={minerals}
                        mineralId={mineralId}
                    />
                </div>
            </div>
        </section>
    )
}

export default MineralPage
