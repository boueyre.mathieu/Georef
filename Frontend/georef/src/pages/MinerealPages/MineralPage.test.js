/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import '@testing-library/jest-dom/extend-expect'
import MineralPage from './MineralPage'

const elementMocked = [
    {
        id: 8,
        name: 'Coucou',
        opacity: '1.20',
        chemicalFormula: 'Formule',
        hardness: '2.00',
        density: '2.50',
        picturePath: 'http://localhost:4000/images/Cyan.png1692286788137.png',
        color: 'couleur',
        family_id: 1,
        habitus_id: 2,
        crystalMesh_id: 1,
        description: '',
        user_id: 1,
        isChecked: 1,
        isDisplay: 1,
    },
]

global.fetch = jest.fn()

const mockFetch = fetch

it('function getAllMineralOneFamily, value', async () => {
    mockFetch.mockResolvedValue({
        json: () => Promise.resolve({ elementMocked }),
    })
})

describe('Verify MineralPage', () => {
    test('Should rendering without crash', () => {
        render(
            <Router>
                <MineralPage />
            </Router>,
        )
    })
})

const mockMineralComponent = jest.fn()
const mockFamilyComponent = jest.fn()

jest.mock(
    '../../components/MineralPagesComponents/MineralList',
    () => (props) => {
        mockMineralComponent(props)
        return <mock-mineralComponent />
    },
)

jest.mock(
    '../../components/MineralPagesComponents/FamilyList',
    () => (props) => {
        mockFamilyComponent(props)
        return <mock-familyComponent />
    },
)

test('FamilyPage passed data props', () => {
    render(
        <Router>
            <MineralPage open mineralId={undefined} mineralslist={[]} />
        </Router>,
    )
    expect(mockFamilyComponent).toHaveBeenCalledTimes(1)
    expect(mockMineralComponent).toHaveBeenCalledTimes(1)
})
