/* eslint-disable import/extensions */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable no-unused-expressions */
import { Routes, Route, redirect } from 'react-router-dom'
import { useContext } from 'react'
import Acceuil from './pages/Home/Acceuil'
import Header from './components/Header/Header.js'
import Footer from './components/Footer/Footer'
import Auth from './Auth/auth'
import Information from './pages/InformationUser/Information'
import AddMineral from './pages/AddMineral/AddMineral'
import FamilyPage from './pages/FamilyPage/FamilyPage'
import MineralPage from './pages/MinerealPages/MineralPage'
import Admin from './pages/Admin/Admin'
import Research from './pages/research/Research'
import MentionsLegales from './pages/Mentions/MentionsLegales'
import AuthContext from './context/authContext'

function App() {
    const authContext = useContext(AuthContext)

    return (
        <>
            <Header />
            <Routes>
                <Route path="/" element={<Acceuil />} />
                <Route path="/auth" element={<Auth />} />
                <Route path="/information" element={<Information />} />
                <Route path="/addMineral" element={<AddMineral />} />
                <Route path="/familyMineral" element={<FamilyPage />} />
                <Route path="/family/:family" element={<MineralPage />} />
                <Route
                    path="/family/:family/:mineralId"
                    element={<MineralPage />}
                />
                {parseInt(authContext.userAdmin) === 1 ? (
                    <Route path="/admin" element={<Admin />} />
                ) : (
                    <Route path="/" element={<Acceuil />} />
                )}
                <Route path="/research" element={<Research />} />
                <Route path="/research/:nameLinked" element={<Research />} />
                <Route path="/mentions" element={<MentionsLegales />} />
            </Routes>
            <Footer />
        </>
    )
}

export default App
