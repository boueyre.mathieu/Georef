/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render, screen } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import React from 'react'
import Navbar from './Navbar'

describe('verify navbar', () => {
    test('should rendering without crash', () => {
        render(
            <Router>
                <Navbar />
            </Router>,
        )
    })

    test('should test what does into the state', () => {
        const setStateLocation = jest.fn()

        jest.spyOn(React, 'useState').mockImplementationOnce((initSate) => [
            initSate,
            setStateLocation,
        ])
    })
})
