/* eslint-disable react/no-unescaped-entities */
/* eslint-disable linebreak-style */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable linebreak-style */
import React from 'react'
import pictureLogo from '../../public/832ce5b5-c474-4978-af83-7f128c82f25e-sansfond100x100.png'
import Navbar from './Navbar'
import SearchBar from './SearchBar'

function Header() {
    return (
        <header className=" text-xl w-full fixed top-0">
            <section className="flex items-center justify-around border-b border-gray-400 bg-white mb-[-0.5rem] mt-[-0.5rem]">
                <img src={pictureLogo} alt="logo" />
                <Navbar />
            </section>
            <section className="bg-white border-b shadow-md shadow-neutral-500/30 rounded-none searchbar border-t border-gray-400">
                <SearchBar />
            </section>
        </header>
    )
}

export default Header
