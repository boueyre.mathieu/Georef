/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import Header from './Header'

describe('verify Header', () => {
    test('should rendering without crash', () => {
        render(
            <Router>
                <Header />
            </Router>,
        )
    })
})
