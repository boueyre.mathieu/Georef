/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useRef } from 'react'
import { Link, useNavigate, useLocation } from 'react-router-dom'

function SearchBar() {
    const [keyword, setKeyword] = useState()
    const navigate = useNavigate()
    const nameInputRef = useRef(null)
    const currentLocation = useLocation()

    const handleSubmit = (e) => {
        e.preventDefault()
        setKeyword(null)
        nameInputRef.current.value = ''
        if (keyword) {
            const tmp = keyword
            setKeyword(null)
            navigate(`/research/${tmp}`)
        } else {
            navigate('/research')
        }
    }

    return (
        <div className="lg:flex justify-around h-[4rem] lg:items-center text-center searchbar_design">
            <div className="">
                <Link to="/familyMineral">Famille minéralogique</Link>
            </div>
            {!currentLocation.pathname.includes('/research') && (
                <form className="search_searchbar" onSubmit={handleSubmit}>
                    <label htmlFor="" className="mr-4">
                        Rechercher
                    </label>
                    <input
                        type="text"
                        id="reherche"
                        name="recherche"
                        ref={nameInputRef}
                        onChange={(e) => setKeyword(e.target.value)}
                        placeholder="Nom d'un minéral"
                        className="bg-gray-200 text-start p-[0.40rem] rounded-md search_searchbar_input"
                    />
                </form>
            )}
        </div>
    )
}

export default SearchBar
