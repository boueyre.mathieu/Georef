/* eslint-disable eqeqeq */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useContext } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import AuthContext from '../../context/authContext'

function Navbar() {
    const location = useLocation()
    const [locationNow, SetLocationNow] = useState(location.pathname === '/')
    const [isNavOpen, setIsNavOpen] = useState(false)
    const navigate = useNavigate()

    const authContext = useContext(AuthContext)
    const { isLoggedIn } = authContext
    const isAdmin = authContext.userAdmin

    function handleDisconnexion() {
        SetLocationNow(location.pathname)
        if (locationNow !== location.pathname) {
            setIsNavOpen((hidemenu) => !hidemenu)
        }
        authContext.logout()
        navigate('/')
    }

    function hiddenMenuClick() {
        SetLocationNow(location.pathname)
        if (locationNow !== location.pathname) {
            setIsNavOpen((hidemenu) => !hidemenu)
        }
    }
    return (
        <nav className="lg:relative top-[12px]">
            <section className="MOBILE-MENU flex lg:hidden pr-5">
                <div
                    className="HAMBURGER-ICON space-y-2"
                    onClick={() => setIsNavOpen((prev) => !prev)}
                >
                    <span className="block h-0.5 w-8 animate-pulse bg-gray-600" />
                    <span className="block h-0.5 w-8 animate-pulse bg-gray-600" />
                    <span className="block h-0.5 w-8 animate-pulse bg-gray-600" />
                </div>
                <div className={isNavOpen ? 'showMenuNav' : 'hideMenuNav'}>
                    <div
                        className="absolute top-0 right-0 px-8 py-8"
                        onClick={() => setIsNavOpen(false)}
                    >
                        <svg
                            className="h-8 w-8 text-gray-600"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        >
                            <line x1="18" y1="6" x2="6" y2="18" />
                            <line x1="6" y1="6" x2="18" y2="18" />
                        </svg>
                    </div>
                    <ul className="flex flex-col justify-between min-h-[250px] items-center">
                        {location.pathname !== '/' && (
                            <li
                                className="border-b border-gray-600 my-8 uppercase"
                                onClick={() => {
                                    hiddenMenuClick()
                                }}
                            >
                                <Link to="/">Accueil</Link>
                            </li>
                        )}
                        {location.pathname !== '/research' && (
                            <li
                                className="border-b border-gray-600 uppercase"
                                onClick={() => {
                                    hiddenMenuClick()
                                }}
                            >
                                <Link to="/research">Recherche</Link>
                            </li>
                        )}
                        {location.pathname !== '/addMineral' &&
                            isLoggedIn &&
                            isLoggedIn && (
                                <li
                                    className="border-b border-gray-600 my-8 uppercase"
                                    onClick={() => {
                                        hiddenMenuClick()
                                    }}
                                >
                                    <Link to="/addMineral">
                                        Ajouter un minéral
                                    </Link>
                                </li>
                            )}
                        {location.pathname !== '/admin' &&
                            isLoggedIn &&
                            isAdmin == 1 && (
                                <li
                                    className="border-b border-gray-600 my-8 uppercase"
                                    onClick={() => {
                                        hiddenMenuClick()
                                    }}
                                >
                                    <Link to="/admin">Page Administrateur</Link>
                                </li>
                            )}
                        {!location.pathname != '/information' && isLoggedIn && (
                            <li
                                className="border-b border-gray-600 my-8 uppercase"
                                onClick={() => {
                                    hiddenMenuClick()
                                }}
                            >
                                <Link to="/information">Vos informations</Link>
                            </li>
                        )}
                        <li className="m-auto">
                            {!isLoggedIn && (
                                <Link to="/auth">
                                    <button
                                        onClick={() => {
                                            hiddenMenuClick()
                                        }}
                                        className="ml-8 bg-gray-600 text-white relative rounded-md top-[-12px] font-bold p-3 hover:opacity-80 hover:shadow-sm hover:shadow-gray-700"
                                    >
                                        Connexion
                                    </button>
                                </Link>
                            )}
                            {isLoggedIn && (
                                <button
                                    onClick={() => {
                                        handleDisconnexion()
                                    }}
                                    className="bg-gray-600 text-white relative rounded-md top-[-12px] font-bold p-3 hover:opacity-80 hover:shadow-sm hover:shadow-gray-700 mt-12"
                                >
                                    Deconnexion
                                </button>
                            )}
                        </li>
                    </ul>
                </div>
            </section>

            <ul className="DESKTOP-MENU hidden space-x-8 lg:flex items-center mt-[-20px] text-center">
                {location.pathname !== '/' && (
                    <li className="hover:border-b border-gray-600">
                        <Link to="/">Accueil</Link>
                    </li>
                )}
                {location.pathname !== '/research' && (
                    <li className="hover:border-b border-gray-600">
                        <Link to="/research">Recherche</Link>
                    </li>
                )}
                {location.pathname !== '/addMineral' && isLoggedIn && (
                    <li className="hover:border-b border-gray-600">
                        <Link to="/addMineral">Ajouter un minéral</Link>
                    </li>
                )}
                {location.pathname !== '/admin' &&
                    isLoggedIn &&
                    isAdmin == 1 && (
                        <li className="hover:border-b border-gray-600">
                            <Link to="/admin" className="flex flex-col-reverse">
                                Page Administrateur{' '}
                            </Link>
                        </li>
                    )}
                {location.pathname !== '/information' && isLoggedIn && (
                    <li className="hover:border-b border-gray-600">
                        <Link to="/information">Vos informations</Link>
                    </li>
                )}
                <li className="m-auto mt-[20px]">
                    {!isLoggedIn && (
                        <Link to="/auth">
                            <button
                                onClick={() => {
                                    hiddenMenuClick()
                                }}
                                className="ml-8 bg-gray-600 text-white relative rounded-md top-[-12px] font-bold p-3 hover:opacity-80 hover:shadow-sm hover:shadow-gray-700"
                            >
                                Connexion
                            </button>
                        </Link>
                    )}
                    {isLoggedIn && (
                        <button
                            onClick={() => {
                                handleDisconnexion()
                            }}
                            className="ml-8 bg-gray-600 text-white relative rounded-md top-[-12px] font-bold p-3 hover:opacity-80 hover:shadow-sm hover:shadow-gray-700"
                        >
                            Deconnexion
                        </button>
                    )}
                </li>
            </ul>
        </nav>
    )
}

export default Navbar
