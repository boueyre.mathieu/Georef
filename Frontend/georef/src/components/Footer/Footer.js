/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React from 'react'
import { Link } from 'react-router-dom'

function Footer() {
    return (
        <footer className="h-[4rem] flex text-md items-center  border-t  shadow-slate-500/50 lg:justify-center absolute bottom-0 w-full">
            <Link
                to="/mentions"
                className="mr-[12rem] hover:opacity-60 font-bold"
            >
                Mentions Légales
            </Link>
        </footer>
    )
}

export default Footer
