/* eslint-disable no-use-before-define */
/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render, screen } from '@testing-library/react'
import {
    afterEach,
    beforeEach,
    describe,
    expect,
    jest,
    test,
} from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import GetCrystalMesh from './GetCrystalMesh'

const crystalMeschMock = [
    {
        id: 1,
        label: 'Cubique',
        description: null,
    },
    {
        id: 2,
        label: 'Hexagonal',
        description: null,
    },
    {
        id: 3,
        label: 'Rhomboédrique',
        description: null,
    },
    {
        id: 4,
        label: 'Tetragonal',
        description: null,
    },
    {
        id: 5,
        label: 'Orthorombique',
        description: null,
    },
    {
        id: 6,
        label: 'Monoclinique',
        description: null,
    },
    {
        id: 7,
        label: 'Triclinique',
        description: null,
    },
]

let windowFetchSpy

beforeEach(() => {
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch)
})

afterEach(() => {
    jest.resetAllMocks()
})

export default async function mockFetch(url) {
    if (
        url ===
        `${process.env.REACT_APP_DB_CONNECT}/api/mineral/getAllCrystalMesh`
    ) {
        return {
            ok: true,
            status: 200,
            json: async () => crystalMeschMock,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

describe('verify getCrystalMesh Component', () => {
    test('should renderinf without crash', () => {
        render(
            <Router>
                <GetCrystalMesh open getValue={Number} value={Number} />
            </Router>,
        )
    })
})
