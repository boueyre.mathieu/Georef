/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable prettier/prettier */
import React, { useEffect, useState, useCallback } from 'react'

function GetCrystalMesh({ getValue, value }) {
    const [data, setData] = useState([])

    const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllCrystalMesh`

    const getCrystalMesh = useCallback(async () => {
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application.json',
                },
            })

            const getDataJSON = await getData.json()
            setData(getDataJSON)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [data])

    useEffect(() => {
        getCrystalMesh()
    }, [])

    const defaultMesh = []

    if (value) {
        return (
            <div>
                <select
                    name="crystalMesh"
                    className="bg-slate-200 ml-[3.40rem] rounded-md p-2 input_modify w-[15.7rem] "
                    onChange={(e) => {
                        getValue(e.target.selectedOptions[0].id)
                    }}
                >
                    <option
                        defaultValue={data.find((e) => {
                            if (e.id === value) {
                                defaultMesh.push(e.id)
                                defaultMesh.push(e.label)
                            }
                        })}
                        id={defaultMesh[0]}
                    >
                        {defaultMesh[1]}
                    </option>
                    {data.map((mesh) => (
                        <option value={mesh.label} id={mesh.id} key={mesh.id}>
                            {mesh.label}
                        </option>
                    ))}
                </select>
            </div>
        )
    }
    return (
        <div>
            <select
                name="crystalMesh"
                className="bg-slate-200 ml-[3.40rem] rounded-md input_addMineral p-[0.30rem] pl-[0.5rem] w-[16rem]"
                onChange={(e) => {
                    getValue(e.target.selectedOptions[0].id)
                }}
            >
                <option value="vide" />
                {data.map((mesh) => (
                    <option value={mesh.label} id={mesh.id} key={mesh.id}>
                        {mesh.label}
                    </option>
                ))}
            </select>
        </div>
    )
}

export default GetCrystalMesh
