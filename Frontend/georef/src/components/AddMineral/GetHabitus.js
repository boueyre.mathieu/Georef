/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useEffect, useState, useCallback } from 'react'

function GetHabitus({ getValue, value }) {
    const [data, setData] = useState([])

    const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllHabitus`

    const getHabitus = useCallback(async () => {
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application.json',
                },
            })

            const getDataJSON = await getData.json()
            setData(getDataJSON)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [data])

    useEffect(() => {
        getHabitus()
    }, [])

    const defaultHabitus = []

    if (value) {
        return (
            <div>
                <select
                    name="habitus"
                    className="bg-slate-200 ml-[2rem] rounded-md p-2 input_modify w-[15.6rem]"
                    onChange={(e) => {
                        getValue(e.target.selectedOptions[0].id)
                    }}
                >
                    <option
                        defaultValue={data.find((e) => {
                            if (e.id === value) {
                                defaultHabitus.push(e.id)
                                defaultHabitus.push(e.label)
                            }
                        })}
                        id={defaultHabitus[0]}
                    >
                        {defaultHabitus[1]}
                    </option>
                    {data.map((habitus) => (
                        <option
                            value={habitus.label}
                            id={habitus.id}
                            key={habitus.id}
                        >
                            {habitus.label}
                        </option>
                    ))}
                </select>
            </div>
        )
    }
    return (
        <div>
            <select
                name="habitus"
                id="habitus"
                onChange={(e) => {
                    getValue(e.target.selectedOptions[0].id)
                }}
                className="bg-slate-200 rounded-md ml-[1.8rem] input_addMineral p-[0.30rem] pl-[0.5rem] w-[16rem]"
            >
                <option value="vide" />
                {data.map((habitus) => (
                    <option
                        value={habitus.label}
                        id={habitus.id}
                        key={habitus.id}
                    >
                        {habitus.label}
                    </option>
                ))}
            </select>
        </div>
    )
}

export default GetHabitus
