/* eslint-disable no-use-before-define */
/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import GetFamily from './GetFamily'

const familyMochedData = [
    {
        id: 1,
        label: 'Éléments Natifs',
        description: null,
        familyPicture:
            'https://www.france-mineraux.fr/wp-content/uploads/2018/11/pierre-cuivre.jpg',
    },
    {
        id: 2,
        label: 'Sulfures et sulfosels',
        description: null,
        familyPicture:
            'https://thumbs.dreamstime.com/b/quartz-avec-des-sulfures-sur-un-fond-blanc-69649063.jpg',
    },
    {
        id: 3,
        label: 'Halogénures et oxydes',
        description: null,
        familyPicture:
            'https://www.madagascandirect.com/uploads/products/rainbow-fluorite-8u.jpg',
    },
    {
        id: 4,
        label: 'Carbonates et borates',
        description: null,
        familyPicture:
            'https://cdn.webshopapp.com/shops/281026/files/360444246/650x750x2/azurite-malachite-brute.jpg',
    },
    {
        id: 5,
        label: 'Sulfates',
        description: null,
        familyPicture:
            'https://lamine2tout.com/14500-large_default/calcedoine-bleue-environs-de-rodez-aveyron-france.jpg',
    },
    {
        id: 6,
        label: 'Phosphates',
        description: null,
        familyPicture:
            'https://media.istockphoto.com/id/666934596/fr/photo/phosphorite-de-pierre-min%C3%A9rale-ou-phosphate-de-roche-isol%C3%A9-sur-fond-blanc-la-phosphorite.jpg?s=170667a&w=0&k=20&c=B5hwURZ3hcH79U-PlznI-Bz55vgLyNhZ1YJrFX5lSY4=',
    },
    {
        id: 7,
        label: 'Silicates et composé organiques',
        description: null,
        familyPicture:
            'https://www.bracelet-chakra-blog.fr/wp-content/uploads/2019/09/pierre-grenat-pyrope.jpg',
    },
]

let windowFetchSpy

beforeEach(() => {
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch)
})

afterEach(() => {
    jest.resetAllMocks()
})

export default async function mockFetch(url) {
    if (
        url === `${process.env.REACT_APP_DB_CONNECT}/api/mineral/getAllFamily`
    ) {
        return {
            ok: true,
            status: 200,
            json: async () => familyMochedData,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

describe('verify getCrystalMesh Component', () => {
    test('should renderinf without crash', () => {
        render(
            <Router>
                <GetFamily />
            </Router>,
        )
    })
})
