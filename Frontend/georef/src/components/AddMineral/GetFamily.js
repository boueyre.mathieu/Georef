/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useEffect, useState, useCallback } from 'react'

function GetFamily({ getValue, value }) {
    const [data, setData] = useState([])

    const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllFamily`

    const getFamily = useCallback(async () => {
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application.json',
                },
            })

            const getDataJSON = await getData.json()
            setData(getDataJSON)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [data])

    useEffect(() => {
        getFamily()
    }, [])

    const defaultFamily = []

    if (value) {
        return (
            <div>
                <select
                    name="family"
                    className="bg-slate-200 ml-[2.2rem] rounded-md p-2 input_modify p-[0.30rem] pl-[0.5rem]"
                    onChange={(e) => {
                        getValue(e.target.selectedOptions[0].id)
                    }}
                >
                    <option
                        defaultValue={data.find((e) => {
                            if (e.id === value) {
                                defaultFamily.push(e.id)
                                defaultFamily.push(e.label)
                            }
                        })}
                        id={defaultFamily[0]}
                    >
                        {defaultFamily[1]}
                    </option>
                    {data.map((family) => (
                        <option
                            value={family.label}
                            id={family.id}
                            key={family.id}
                        >
                            {family.label}
                        </option>
                    ))}
                </select>
            </div>
        )
    }
    return (
        <div>
            <select
                name="familyMineral"
                id="familyMineral"
                onChange={(e) => {
                    getValue(e.target.selectedOptions[0].id)
                }}
                className="bg-slate-200 rounded-md ml-[2.10rem] input_addMineral p-[0.30rem] pl-[1rem]"
            >
                <option value="vide" />
                {data.map((family) => (
                    <option value={family.label} id={family.id} key={family.id}>
                        {family.label}
                    </option>
                ))}
            </select>
        </div>
    )
}

export default GetFamily
