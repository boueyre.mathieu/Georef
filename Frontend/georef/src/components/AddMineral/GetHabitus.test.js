/* eslint-disable no-use-before-define */
/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import GetHabitus from './GetHabitus'

const habitusMockedData = [
    {
        id: 1,
        label: 'Ariculaire',
    },
    {
        id: 2,
        label: 'Foliacé',
    },
    {
        id: 3,
        label: 'Botryoïde',
    },
    {
        id: 4,
        label: 'Dendritique',
    },
    {
        id: 5,
        label: 'Fibreux',
    },
    {
        id: 6,
        label: 'Lamellaire',
    },
    {
        id: 7,
        label: 'Mamelonné',
    },
    {
        id: 8,
        label: 'Massif',
    },
    {
        id: 9,
        label: 'Rayonnant',
    },
    {
        id: 10,
        label: 'Réniforme',
    },
    {
        id: 11,
        label: 'Tabulaire',
    },
    {
        id: 12,
        label: 'Bacillaire',
    },
]

let windowFetchSpy

beforeEach(() => {
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch)
})

afterEach(() => {
    jest.resetAllMocks()
})

export default async function mockFetch(url) {
    if (
        url === `${process.env.REACT_APP_DB_CONNECT}/api/mineral/getAllHabitus`
    ) {
        return {
            ok: true,
            status: 200,
            json: async () => habitusMockedData,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

describe('verify getCrystalMesh Component', () => {
    test('should renderinf without crash', () => {
        render(
            <Router>
                <GetHabitus />
            </Router>,
        )
    })
})
