/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import FamilyPageComponent from './FamilyPageComponent'

describe('verify FamilyPageComponent', () => {
    test('page rendering without crash', () => {
        render(
            <Router>
                <FamilyPageComponent />
            </Router>,
        )
    })
})
