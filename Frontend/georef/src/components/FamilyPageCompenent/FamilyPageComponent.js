/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React from 'react'
import { Link } from 'react-router-dom'

function FamilyPageComponent({ link, family, picture }) {
    return (
        <div>
            <Link to={link}>
                <div className="h-[20rem] mr-8 w-[20rem] text-center mb-8 rounded-xl hover:shadow hover:shadow-neutral-600/50 shadow shadow-slate-500/50 family_component">
                    <img
                        src={picture}
                        alt="image du minéral de référence"
                        className="h-[225px] m-auto"
                    />
                    <p>Famille: {family}</p>
                </div>
            </Link>
        </div>
    )
}

export default FamilyPageComponent
