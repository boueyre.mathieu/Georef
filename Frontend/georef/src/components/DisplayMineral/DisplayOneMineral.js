/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useEffect, useState, useCallback } from 'react'

const getCrystalMesh = async (mineral, setCrystalMesh) => {
    const crystalUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/displayOneCrystalMesh/${mineral.crystalMesh_id}`
    try {
        const getData = await fetch(crystalUrl, {
            method: 'GET',
            headers: {
                'content-type': 'application.json',
            },
        })

        const getDataJSON = await getData.json()
        setCrystalMesh(getDataJSON)
    } catch {
        console.log("Pas de réponse de l'API")
    }
}

const getFamily = async (mineral, setFamily) => {
    const crystalUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/displayOneFamily/${mineral.family_id}`
    try {
        const getData = await fetch(crystalUrl, {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
            },
        })

        const getDataJSON = await getData.json()
        setFamily(getDataJSON)
    } catch {
        console.log("Pas de réponse de l'API")
    }
}

const getHabitus = async (mineral, setHabitus) => {
    const crystalUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/displayOneHabitus/${mineral.habitus_id}`
    try {
        const getData = await fetch(crystalUrl, {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
            },
        })

        const getDataJSON = await getData.json()
        setHabitus(getDataJSON)
    } catch {
        console.log("Pas de réponse de l'API")
    }
}

function DisplayOneMineral({ id }) {
    const [mineral, setMineral] = useState([])
    const [crystalMesh, setCrystalMesh] = useState([])
    const [family, setFamily] = useState([])
    const [habitus, setHabitus] = useState([])
    const [oldId, setOldId] = useState()

    const mineralUrl = `${process.env.REACT_APP_DB_CONNECT}/mineral/DisplayOneMineral/${id}`

    const getOneMineral = useCallback(async () => {
        try {
            const getData = await fetch(mineralUrl, {
                method: 'GET',
                headers: {
                    'Content-type': 'application/json',
                },
            })

            const getDataJSON = await getData.json()
            setMineral(getDataJSON)
            getCrystalMesh(getDataJSON, setCrystalMesh)
            getFamily(getDataJSON, setFamily)
            getHabitus(getDataJSON, setHabitus)
            setOldId(id)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }, [mineral])

    useEffect(() => {
        getOneMineral()
    }, [])

    if (id !== oldId) {
        getOneMineral()
    }

    return (
        <div className="mt-[1rem] mb-[12rem] design_display">
            <div className="flex w-auto justify-center display_mineral">
                <div className="flex justify-start items-start flex-col text-left display_img_field">
                    <p className="font-bold">Image du minéral</p>
                    <img
                        src={mineral.picturePath}
                        alt="Image Téléchargée"
                        className="w-[300px] display_mineral_img"
                    />
                </div>
                <div className="text-justify ml-12 marginl_display_configuration">
                    <div className="mr-12 margin_display_configuration">
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p id="name" className="mr-[2.5rem] font-bold">
                                Nom du minéral
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {mineral.name}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p
                                id="familyMineral"
                                className="mr-[1.50rem] font-bold"
                            >
                                Famille du minéral
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {family.label}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p
                                id="crystalMesh"
                                className="mr-[2.75rem] font-bold"
                            >
                                Maille cristaline
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {crystalMesh.label}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p
                                id="chimicalFormula"
                                className="mr-[1.70rem] font-bold"
                            >
                                Formule chimique
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {mineral.chemicalFormula}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p id="habitus" className="mr-[1.25rem] font-bold">
                                Habitus du minéral
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {habitus.label}
                            </p>
                        </div>
                    </div>
                    <div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p id="color" className="mr-[1.25rem] font-bold">
                                Couleur du minéral
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {mineral.color}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p id="opacity" className="mr-[1.35rem] font-bold">
                                Opacité du minéral
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {mineral.opacity}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p id="glow" className="mr-[8.10rem] font-bold">
                                Éclat
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {mineral.glow}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p id="hardness" className="mr-[1.75rem] font-bold">
                                Dureté du minéral
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {mineral.hardness}
                            </p>
                        </div>
                        <div className="mb-2 flex items-center field_organisation_displayMineral">
                            <p id="density" className="mr-[1.5rem] font-bold">
                                Densité du minéral
                            </p>
                            <p className="bg-slate-300 p-3 rounded-md">
                                {mineral.density}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex flex-col w-[50rem] m-auto description_displayMineral ">
                <p className="mb-3 text-left mt-8 font-bold">
                    Description du minéral
                </p>
                <p className="bg-slate-200 h-[10rem] p-5 rounded-xl">
                    {mineral.description}
                </p>
            </div>
        </div>
    )
}

export default DisplayOneMineral
