/* eslint-disable no-use-before-define */
/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render, screen } from '@testing-library/react'
import {
    afterEach,
    beforeEach,
    describe,
    expect,
    jest,
    test,
} from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import DisplayOneMineral from './DisplayOneMineral'

const displayIOneFamilyMock = {
    id: 1,
    label: 'Éléments Natifs',
    description: null,
    familyPicture:
        'https://www.france-mineraux.fr/wp-content/uploads/2018/11/pierre-cuivre.jpg',
}

const displayOneHabitus = {
    id: 1,
    label: 'Ariculaire',
}

const displayOneCrystalMeshMock = {
    id: 1,
    label: 'Cubique',
    description: null,
}

let windowFetchSpy

beforeEach(() => {
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch)
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch2)
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch3)
})

afterEach(() => {
    jest.resetAllMocks()
})

async function mockFetch(url) {
    if (
        url ===
        `${process.env.REACT_APP_DB_CONNECT}/api/mineral/displayOneFamily/1`
    ) {
        return {
            ok: true,
            status: 200,
            json: async () => displayIOneFamilyMock,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

async function mockFetch2(url) {
    if (
        url ===
        `${process.env.REACT_APP_DB_CONNECT}0/api/mineral/displayOneHabitus/1`
    ) {
        return {
            ok: true,
            status: 200,
            json: async () => displayOneHabitus,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

async function mockFetch3(url) {
    if (
        url ===
        `${process.env.REACT_APP_DB_CONNECT}/api/mineral/displayOneCrystalMesh/1`
    ) {
        return {
            ok: true,
            status: 200,
            json: async () => displayOneCrystalMeshMock,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

describe('verify display component', () => {
    test('should rendering without crash', () => {
        render(
            <Router>
                <DisplayOneMineral />
            </Router>,
        )
    })
})
