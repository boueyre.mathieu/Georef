/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import MineralList from './MineralList'

describe('verify MineralList page', () => {
    test('should rendering without crash', () => {
        render(
            <Router>
                <MineralList open mineralsList={[]} />
            </Router>,
        )
    })
})
