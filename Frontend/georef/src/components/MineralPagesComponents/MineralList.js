/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from 'react'
import DisplayOneMineral from '../DisplayMineral/DisplayOneMineral'

function MineralList({ mineralsList, mineralId }) {
    const [id, setId] = useState(mineralId)

    return (
        <div className="h-[8rem] ml-3 width_organisation">
            <div className=" p-5 w-full width_organisation h-full rounded-3xl shadow-md shadow-neutral-500/50">
                <h4 className="mb-5 font-bold">Minéraux</h4>
                <select
                    className="p-2 rounded-xl"
                    onChange={(e) => setId(e.target.value)}
                >
                    <option value="vide" />
                    {mineralsList.map((mineral) => (
                        <option
                            key={mineral.id}
                            value={mineral.id}
                            className="hover:font-bold mb-3"
                        >
                            {mineral.name}
                        </option>
                    ))}
                </select>
            </div>
            <div className="m-auto organisation_displayMineral">
                {id && <DisplayOneMineral id={id} />}
            </div>
        </div>
    )
}

export default MineralList
