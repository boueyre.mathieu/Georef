/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

function FamilyList({ familyParams }) {
    const [families, setFamilies] = useState([])
    const navigate = useNavigate()
    const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/getAllFamily`

    const getFamily = async () => {
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application.json',
                },
            })

            const getDataJSON = await getData.json()
            setFamilies(getDataJSON)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }

    function changeUrl(value) {
        navigate(`/family/${value}`)
        value = ''
    }

    useEffect(() => {
        getFamily()
    }, [])

    return (
        <div className=" w-[50rem] h-[8rem] ml-3 p-5 rounded-3xl shadow-md shadow-neutral-500/50 m-0 width_organisation">
            <h4 className="mb-5 font-bold">Familles minéralogique</h4>
            <select
                onChange={(e) => {
                    changeUrl(e.target.value)
                }}
                className="p-2 rounded-xl"
            >
                <option value={familyParams}>{familyParams}</option>
                {families.map((family) => (
                    <option
                        className="mb-3 hover:font-bold"
                        value={family.label}
                        key={family.id}
                    >
                        {family.label}
                    </option>
                ))}
            </select>
        </div>
    )
}

export default FamilyList
