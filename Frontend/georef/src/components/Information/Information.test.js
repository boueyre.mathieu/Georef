/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-undef */
import '@testing-library/jest-dom/extend-expect'
import { test } from '@jest/globals'
import { render } from '@testing-library/react'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import InformationGet from './InformationGet'

// const familyMockedData = [

// ]

const server = setupServer(
    rest.get(`${process.env.REACT_APP_DB_CONNECT}/auth/user`, (req, res, ctx) =>
        res(ctx.json({ familyList: userMockedData })),
    ),
)

beforeAll(() => server.listen())
afterEach(() => server.restoreHandlers())
afterAll(() => server.close())

test('Should render without crash', () => {
    render(<InformationGet />)
})
