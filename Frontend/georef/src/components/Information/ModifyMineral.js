/* eslint-disable camelcase */
/* eslint-disable react/jsx-curly-brace-presence */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState, useContext } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import AuthContext from '../../context/authContext'
import GetCrystalMesh from '../AddMineral/GetCrystalMesh'
import GetFamily from '../AddMineral/GetFamily'
import GetHabitus from '../AddMineral/GetHabitus'

function ModifyMineral({ mineralModify }) {
    const { handleSubmit } = useForm()
    const navigate = useNavigate()
    // const [mineral, setMineral] = useState([])
    const [name, setName] = useState(mineralModify.name)
    const [color, setColor] = useState(mineralModify.color)
    const [formula, setFormula] = useState(mineralModify.chemicalFormula)
    const [description, setDescription] = useState(mineralModify.description)
    const [glow, setGlow] = useState(mineralModify.glow)
    const [opacity, setOpacity] = useState(mineralModify.opacity)
    const [density, setDensity] = useState(mineralModify.density)
    const [hardness, setHardness] = useState(mineralModify.hardness)
    const [family_id, setFamily_id] = useState(mineralModify.family_id)
    const [mesh_id, setMesh_id] = useState(mineralModify.crystalMesh_id)
    const [habitus_id, setHabitus_id] = useState(mineralModify.habitus_id)

    const [picture, setPicture] = useState(mineralModify.picturePath)
    const [pictureDisplay, setPictureDisplay] = useState(
        mineralModify.picturePath,
    )

    const setNewHabitus = (e) => {
        setHabitus_id(parseInt(e))
    }

    const setNewFamily = (e) => {
        setFamily_id(parseInt(e))
    }

    const setNewMesh = (e) => {
        setMesh_id(parseInt(e))
    }

    const authContext = useContext(AuthContext)

    const onSubmit = () => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('crystalMesh', mesh_id)
        formData.append('color', color)
        formData.append('chemicalFormula', formula)
        formData.append('description', description)
        formData.append('family', family_id)
        formData.append('glow', glow)
        formData.append('opacity', opacity)
        formData.append('density', density)
        formData.append('hardness', hardness)
        formData.append('image', picture)
        formData.append('habitus', habitus_id)

        const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/updateMineral/${mineralModify.id}`
        const submitData = async () => {
            try {
                const mineralForm = await fetch(url, {
                    method: 'POST',
                    body: formData,
                    headers: {
                        Accept: 'multipart/form-data',
                        Authorization: `Bearer ${authContext.token}`,
                    },
                })
                const dataMineralForm = await mineralForm.json()
                // if (dataMineralForm) {
                //     window.location.reload()
                // }
                console.log(dataMineralForm)
                navigate('/information')
            } catch {
                console.log("Pas de réponse de l'API")
            }
        }

        submitData()
    }

    function displayPicture(picture) {
        console.log(picture[0])
        const pictureUpload = URL.createObjectURL(picture)
        setPictureDisplay(pictureUpload)
    }

    return (
        <div>
            <form
                onSubmit={handleSubmit(onSubmit)}
                className="flex flex-col text-center w-full justify-center items-center m-auto mt-8 form_modify"
            >
                <div className="flex form_modify_organization">
                    <div className="flex flex-col justify-start items-start text-left w-auto">
                        <label htmlFor="picture" className="font-bold mb-5">
                            Image du minéral
                        </label>
                        <input
                            type="file"
                            id="picture"
                            name="picture"
                            onChange={(e) => {
                                displayPicture(e.target.files[0])
                                setPicture(e.target.files[0])
                            }}
                        />
                        {picture && (
                            <img
                                src={pictureDisplay}
                                alt="Image Téléchargée"
                                className="w-[400px]"
                            />
                        )}
                    </div>
                    <div className="flex-col text-justify ml-12 mt-10 field_modify modify_text_organization">
                        <div className="mb-2 field_modify">
                            <label
                                htmlFor="name"
                                id="name"
                                className="font-bold"
                            >
                                Nom du minéral
                            </label>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                defaultValue={name}
                                className="bg-slate-200 rounded-md ml-[3.25rem] p-2 input_modify w-[15.65rem]"
                                onChange={(e) => {
                                    setName(e.target.value)
                                }}
                            />
                        </div>
                        <div className="flex mb-2 items-center field_modify">
                            <label
                                htmlFor="familyMineral"
                                id="familyMineral"
                                className="font-bold"
                            >
                                Famille du minéral
                            </label>
                            <GetFamily
                                getValue={setNewFamily}
                                value={mineralModify.family_id}
                            />
                        </div>
                        <div className="flex mb-2 items-center field_modify">
                            <label
                                htmlFor="crystalmesh"
                                id="crystalMesh"
                                className="font-bold"
                            >
                                Maille cristaline
                            </label>
                            <GetCrystalMesh
                                getValue={setNewMesh}
                                value={mineralModify.crystalMesh_id}
                            />
                        </div>
                        <div className="mb-2 items-center field_modify">
                            <label
                                htmlFor="chimicalFormula"
                                id="chimicalFormula"
                                className="font-bold"
                            >
                                Formule chimique
                            </label>
                            <input
                                type="text"
                                name="chimicalFormula"
                                defaultValue={formula}
                                onChange={(e) => {
                                    setFormula(e.target.value)
                                }}
                                className="bg-slate-200 ml-[2.3rem] rounded-md p-2 input_modify w-[15.75]"
                            />
                        </div>
                        <div className="flex mb-2 items-center field_modify">
                            <label
                                htmlFor="habitus"
                                id="habitus"
                                className="font-bold"
                            >
                                Habitus du minéral
                            </label>
                            <GetHabitus
                                getValue={setNewHabitus}
                                value={mineralModify.habitus_id}
                            />
                        </div>
                        <div className="mb-2 items-center field_modify">
                            <label
                                htmlFor="color"
                                id="color"
                                className="font-bold"
                            >
                                Couleur du minéral
                            </label>
                            <input
                                type="text"
                                name="color"
                                id="color"
                                defaultValue={color}
                                onChange={(e) => {
                                    setColor(e.target.value)
                                }}
                                className="bg-slate-200 rounded-md ml-[1.85rem] p-2 input_modify w-[15.7rem]"
                            />
                        </div>
                        <div className="mb-2 items-center field_modify">
                            <label
                                htmlFor="opacity"
                                id="opacity"
                                className="font-bold"
                            >
                                Opacité du minéral
                            </label>
                            <select
                                name="opacity"
                                id="opacity"
                                onChange={(e) => {
                                    setOpacity(e.target.value)
                                }}
                                className="bg-slate-200 rounded-md ml-[1.85rem] p-2 input_modify w-[15.7rem]"
                            >
                                <option defaultValue={opacity}>
                                    {opacity}
                                </option>
                                <option value="transparent">Transparent</option>
                                <option value="semi-transparent">
                                    Semi-transparent
                                </option>
                                <option value="translucide">Translucide</option>
                                <option value="non-transparent">
                                    Non-transparent
                                </option>
                                <option value="opaque">Opaque</option>
                            </select>
                        </div>
                        <div className="mb-2 items-center field_modify">
                            <label
                                htmlFor="glow"
                                id="glow"
                                className="font-bold"
                            >
                                Eclat
                            </label>
                            <select
                                name="glow"
                                id="glow"
                                onChange={(e) => setGlow(e.target.value)}
                                className="bg-slate-200 rounded-md ml-[8.55rem] p-2 input_modify w-[15.7rem]"
                            >
                                <option defaultValue={glow}>{glow}</option>
                                <option value="adamantin">Adamantin</option>
                                <option value="vitreux">Vitreux</option>
                                <option value="resineux">Résineux</option>
                                <option value="nacre">Nacré</option>
                                <option value="soyeux">Soyeux</option>
                                <option value="gras">Gras</option>
                                <option value="terreux">Terreux</option>
                            </select>
                        </div>
                        <div className="mb-2 items-center field_modify">
                            <label
                                htmlFor="hardness"
                                id="hardness"
                                className="font-bold"
                            >
                                Dureté du minéral
                            </label>
                            <input
                                type="number"
                                id="number"
                                name="number"
                                defaultValue={hardness}
                                min="0.5"
                                max="20"
                                step="0.05"
                                onChange={(e) => {
                                    setHardness(e.target.value)
                                }}
                                className="bg-slate-200 rounded-md ml-[2.25rem] p-2 input_modify w-[15.8rem]"
                            />
                        </div>
                        <div className="mb-2 items-center field_modify">
                            <label
                                htmlFor="density"
                                id="density"
                                className="font-bold"
                            >
                                Densité du minéral
                            </label>
                            <input
                                type="number"
                                id="number"
                                defaultValue={density}
                                name="number"
                                min="0.5"
                                max="20"
                                step="0.05"
                                onChange={(e) => {
                                    setDensity(e.target.value)
                                }}
                                className="bg-slate-200 rounded-md ml-[1.85rem] p-2 input_modify w-[15.9rem]"
                            />
                        </div>
                    </div>
                </div>
                <div className="flex flex-col w-full">
                    <label
                        htmlFor="description"
                        className="mb-3 text-left mt-8 font-bold"
                    >
                        Description du minéral
                    </label>
                    <textarea
                        name="description"
                        id="description"
                        cols="30"
                        rows="10"
                        defaultValue={description}
                        onChange={(e) => {
                            setDescription(e.target.value)
                        }}
                        className="bg-slate-200 rounded-md w-full mb-8 p-5 modify-description"
                    />
                </div>
                <button
                    type={'submit'}
                    className="bg-slate-500 p-3 rounded-xl text-white font-bold text-xl mb-[2rem]"
                >
                    Soumettre le minéral
                </button>
            </form>
        </div>
    )
}

export default ModifyMineral
