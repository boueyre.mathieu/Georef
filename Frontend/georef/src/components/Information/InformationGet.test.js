/* eslint-disable no-use-before-define */
/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import InformationGet from './InformationGet'

describe('verify InformationGet Component', () => {
    test('rendering without crash', () => {
        render(
            <Router>
                <InformationGet />
            </Router>,
        )
    })
})
