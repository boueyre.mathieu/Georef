/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useEffect, useState, useContext } from 'react'
import AuthContext from '../../context/authContext'
import DisplayOneMineral from '../DisplayMineral/DisplayOneMineral'

function GetUserMinerals() {
    const [userMinerals, setUserMinerals] = useState([])
    const [idMineral, setIdMineral] = useState()
    const [mineralChoose, setMineralChoose] = useState()

    const authContext = useContext(AuthContext)

    const userMineralsFunction = async () => {
        const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/userMinerals`
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                    Authorization: `Bearer ${authContext.token}`,
                },
            })

            const getDataJSON = await getData.json()
            setUserMinerals(getDataJSON)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }

    const getMineral = async () => {
        const url = `${process.env.REACT_APP_DB_CONNECT}/mineral//userMineralId/${idMineral}`

        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                    Authorization: `Bearer ${authContext.token}`,
                },
            })

            const getDataJSON = await getData.json()
            setMineralChoose(getDataJSON)
            console.log(getDataJSON)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }

    useEffect(() => {
        userMineralsFunction()
    }, [])

    if (idMineral) {
        getMineral()
        setIdMineral()
    }

    return (
        <div>
            <div>
                <div className="w-[30rem] bg-slate-500 p-5 rounded-3xl text-center m-auto">
                    <p className="mb-5 text-white font-bold">
                        Vous avez {userMinerals.length} élément(s) en attente de
                        validation
                    </p>
                    <select
                        name="mineralsUser"
                        id="mineralUser"
                        onChange={(e) => {
                            setIdMineral(e.target.selectedOptions[0].id)
                        }}
                    >
                        <option value="vide" />
                        {userMinerals.map((mineral) => (
                            <option
                                value={mineral.name}
                                id={mineral.id}
                                key={mineral.id}
                            >
                                {mineral.name}
                            </option>
                        ))}
                    </select>
                </div>

                {mineralChoose && <DisplayOneMineral id={mineralChoose.id} />}
            </div>
        </div>
    )
}

export default GetUserMinerals
