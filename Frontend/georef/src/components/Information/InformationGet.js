/* eslint-disable react/jsx-filename-extension */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useContext, useState, useEffect, useCallback } from 'react'
import authContext from '../../context/authContext'

function InformationGet() {
    const [getData, setGetData] = useState([])

    const authCtx = useContext(authContext)

    const getInformation = useCallback(async () => {
        const url = `${process.env.REACT_APP_DB_CONNECT}/auth/user`

        try {
            const dataUser = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                    Authorization: `Bearer ${authCtx.token}`,
                },
            })
            const dataUserResult = await dataUser.json()
            setGetData(dataUserResult)
        } catch {
            ;(error) => console.log(error)
        }
    }, [getData])

    useEffect(() => {
        getInformation()
    }, [])

    return (
        <div className="text-center mb-12 shadow shadow-slate-500/50 w-[20rem] rounded-3xl p-5 m-auto">
            <ul>
                <li data-testid="name">Nom: {getData.firstName}</li>
                <li>Prénom: {getData.lastName}</li>
                <li>Email: {getData.email}</li>
            </ul>
        </div>
    )
}

export default InformationGet
