/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable radix */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
/* eslint-disable eqeqeq */
import React, { useEffect, useState, useContext } from 'react'
import authContext from '../../context/authContext'
import DisplayOneMineral from '../DisplayMineral/DisplayOneMineral'
// import AddMineral from '../../pages/AddMineral/AddMineral'
import ModifyMineral from './ModifyMineral'

function GetMineralsUser() {
    const authCtx = useContext(authContext)
    const [mineralDisplay, setMineralDisplay] = useState([])
    const [idMineral, setIdMineral] = useState()
    const [userMineralsValid, setUserMineralsValid] = useState([])
    const [userMineralsToCheck, setUserMineralsToCheck] = useState([])
    const [userMineralsToModify, setUserMineralsToModify] = useState([])
    const [userMinerals, setUserMinerals] = useState([])
    const [mineralToModify, setMineralToModify] = useState()

    const userMineralsFunction = async () => {
        const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/userMinerals`
        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                    Authorization: `Bearer ${authCtx.token}`,
                },
            })

            const getDataJSON = await getData.json()
            setUserMinerals(getDataJSON)
            setUserMineralsToCheck(
                getDataJSON.filter(
                    (mineral) =>
                        mineral.isChecked == 0 && mineral.isDisplay == 0,
                ),
            )
            setUserMineralsToModify(
                getDataJSON.filter(
                    (mineral) =>
                        mineral.isChecked == 1 && mineral.isDisplay == 0,
                ),
            )
            setUserMineralsValid(
                getDataJSON.filter(
                    (mineral) =>
                        mineral.isChecked == 1 && mineral.isDisplay == 1,
                ),
            )
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }
    useEffect(() => {
        userMineralsFunction()
    }, [])

    const getMineral = async () => {
        const url = `${process.env.REACT_APP_DB_CONNECT}/mineral/userMineralId/${idMineral}`

        try {
            const getData = await fetch(url, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                    Authorization: `Bearer ${authCtx.token}`,
                },
            })

            const getDataJSON = await getData.json()
            setMineralDisplay(getDataJSON)
        } catch {
            console.log("Pas de réponse de l'API")
        }
    }

    const getMineralToDisplay = (value) => {
        if (!value || value.length === 0) {
            setMineralDisplay()
            setMineralToModify()
        } else {
            setIdMineral(parseInt(value))
            setMineralToModify()
        }
    }

    const modifyClick = () => {
        if (mineralDisplay) {
            const mineral = mineralDisplay
            setMineralToModify(mineral)
            setMineralDisplay()
        }
    }

    const deleteClick = async (id) => {
        try {
            await fetch(
                `${process.env.REACT_APP_DB_CONNECT}/mineral/delete/${id}`,
                {
                    method: 'DELETE',
                    headers: {
                        Accept: 'multipart/application/json',
                        Authorization: `Bearer ${authCtx.token}`,
                    },
                },
            )
        } catch {
            console.log("Pas de réponse de l'API")
        }
        setIdMineral('')
        getMineralToDisplay()
        userMineralsFunction()
    }

    if (idMineral) {
        getMineral()
        setIdMineral()
    }

    return (
        <div className="space-y-3">
            {userMineralsToModify.length !== 0 && (
                <div>
                    <div className="w-full bg-slate-500 p-5 rounded-3xl text-center m-auto">
                        <p className="mb-5 text-white font-bold">
                            Vous avez {userMineralsToModify.length} élément(s)
                            en attente de modification(s)
                        </p>
                        <select
                            className="p-2 rounded-xl"
                            name="mineralsUserToModify"
                            id="mineralsUserToModify"
                            onChange={(e) => {
                                getMineralToDisplay(
                                    e.target.selectedOptions[0].id,
                                )
                            }}
                        >
                            <option value="vide" />
                            {userMineralsToModify.map((mineral) => (
                                <option
                                    value={mineral.name}
                                    id={mineral.id}
                                    key={mineral.id}
                                >
                                    {mineral.name}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
            )}
            {userMineralsToCheck.length !== 0 && (
                <div>
                    <div className="w-[full bg-slate-500 p-5 rounded-3xl text-center m-auto">
                        <p className="mb-5 text-white font-bold">
                            Vous avez {userMineralsToCheck.length} élément(s) en
                            attente de validation
                        </p>
                        <select
                            className="p-2 rounded-xl"
                            name="mineralsUserToModify"
                            id="mineralsUserToModify"
                            onChange={(e) => {
                                getMineralToDisplay(
                                    e.target.selectedOptions[0].id,
                                )
                            }}
                        >
                            <option value="vide" />
                            {userMineralsToCheck.map((mineral) => (
                                <option
                                    value={mineral.name}
                                    id={mineral.id}
                                    key={mineral.id}
                                >
                                    {mineral.name}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
            )}
            {userMineralsValid.length !== 0 && (
                <div>
                    <div className="w-full bg-slate-500 p-5 rounded-3xl text-center m-auto">
                        <p className="mb-5 text-white font-bold">
                            Vous avez {userMineralsValid.length} élément(s)
                            validé(s)
                        </p>
                        <select
                            className="p-2 rounded-xl"
                            name="mineralsUserToModify"
                            id="mineralsUserToModify"
                            onChange={(e) => {
                                getMineralToDisplay(
                                    e.target.selectedOptions[0].id,
                                )
                            }}
                        >
                            <option value="vide" />
                            {userMineralsValid.map((mineral) => (
                                <option
                                    value={mineral.name}
                                    id={mineral.id}
                                    key={mineral.id}
                                >
                                    {mineral.name}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
            )}
            {mineralDisplay && mineralDisplay.id && (
                <div className="shadow shadow-slate-500/50 p-5 rounded-xl w-full pb-0">
                    <div className="justify-end flex">
                        <button
                            className="bg-orange-500 hover:bg-orange-700 text-white font-bold py-2 px-2 border border-orange-700 rounded-xl ml-5 flex-col mb-2"
                            onClick={() => {
                                modifyClick()
                            }}
                        >
                            Modifier
                        </button>
                        <button
                            className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-2 border border-red-700 rounded-xl ml-5 flex-col mb-2"
                            onClick={() => {
                                deleteClick(mineralDisplay.id)
                            }}
                        >
                            Supprimer
                        </button>
                    </div>
                    <div className="p-1">
                        <DisplayOneMineral id={mineralDisplay.id} />
                    </div>
                </div>
            )}
            {mineralToModify && mineralToModify.id && (
                <div className="shadow shadow-slate-500/50 p-5 rounded-xl">
                    <ModifyMineral mineralModify={mineralToModify} />
                    <button
                        className="bg-orange-500 hover:bg-orange-700 text-white font-bold py-1 px-2 border border-orange-700 rounded-xl ml-5 flex-col"
                        onClick={() => {
                            setMineralToModify()
                        }}
                    >
                        Annuler
                    </button>
                    <button
                        className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-700 rounded-xl ml-5 flex-col"
                        onClick={() => {
                            deleteClick(mineralDisplay.id)
                        }}
                    >
                        Supprimer
                    </button>
                </div>
            )}
            {userMinerals.length === 0 && (
                <div>
                    <h2>Aucun minéral ajouté</h2>
                </div>
            )}
        </div>
    )
}

export default GetMineralsUser
