/* eslint-disable no-use-before-define */
/* eslint-disable react/function-component-definition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { render } from '@testing-library/react'
import { describe, expect, jest, test } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import GetUserMinerals from './GetUserMinerals'

const getMineralMockData = [
    {
        id: 6,
        name: 'Nouvelle image',
        chemicalFormula: 'une super fomrule',
        hardness: '2.00',
        glow: 'Balala',
        color: 'couleur',
        opacity: '1.20',
        density: '2.50',
        picturePath:
            'http://localhost:4000/images/tÃ©lÃ©chargement.jpg1694080306937.jpg',
        description: '',
        createdAt: '2023-08-16T09:45:18.000Z',
        family_id: 3,
        habitus_id: 2,
        crystalMesh_id: 1,
        user_id: 1,
        isDisplay: false,
        isChecked: false,
    },
]

let windowFetchSpy

beforeEach(() => {
    windowFetchSpy = jest.spyOn(window, 'fetch').mockImplementation(mockFetch)
})

afterEach(() => {
    jest.resetAllMocks()
})

export default async function mockFetch(url) {
    if (url === `${process.env.REACT_APP_DB_CONNECT}/mineral/userMinerals`) {
        return {
            ok: true,
            status: 200,
            json: async () => getMineralMockData,
        }
    }

    throw new Error(`Unhandled request: ${url}`)
}

describe('verify GetUserMinerals component', () => {
    test('should rendering without crash', () => {
        render(
            <Router>
                <GetUserMinerals />
            </Router>,
        )
    })
})
