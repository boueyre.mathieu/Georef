/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/require-default-props */
/* eslint-disable react/jsx-no-constructed-context-values */
/* eslint-disable import/no-extraneous-dependencies */
import React, { createContext, useState } from 'react'
import PropType from 'prop-types'

const defaultValues = {
    token: '',
    userId: null,
    userIsLoggedIn: false,
    userAdmin: false,
    login: () => {},
    logout: () => {},
}

const AuthContext = createContext(defaultValues)

const tokenStorage = localStorage.getItem('token')
const userIdStorage = localStorage.getItem('userId')
const userAdminStorage = localStorage.getItem('userAdmin')

export function AuthContextProvider(props) {
    const [token, setToken] = useState(tokenStorage)
    const [userId, setUserId] = useState(userIdStorage)
    const [userAdmin, setAdmin] = useState(userAdminStorage)

    const loginHandler = (token, id, isAdmin) => {
        setToken(token)
        setUserId(id)
        setAdmin(isAdmin)
        localStorage.setItem('token', token)
        localStorage.setItem('userId', id)
        localStorage.setItem('userAdmin', isAdmin)
    }

    const logoutHandler = () => {
        setToken(null)
        setUserId(null)
        setAdmin(0)
        localStorage.clear()
    }

    // convertie le token en valeur booléene
    const userIsLoggedIn = !!token

    const contextValue = {
        token,
        userId,
        isLoggedIn: userIsLoggedIn,
        userAdmin,
        login: loginHandler,
        logout: logoutHandler,
    }

    return (
        <AuthContext.Provider value={contextValue}>
            {props.children}
        </AuthContext.Provider>
    )
}

AuthContextProvider.propTypes = {
    children: PropType.node,
}

export default AuthContext
