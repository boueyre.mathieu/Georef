/* eslint-disable no-console */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-unescaped-entities */
import React, { useContext, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import AuthContext from '../context/authContext'

function Auth() {
    const [createAccount, SetCreateAccount] = useState(false)
    const { register, handleSubmit } = useForm()
    const [errorAuth, setErrorAuth] = useState(false)
    const navigate = useNavigate()

    const authContext = useContext(AuthContext)

    const createAccountHandle = () => {
        SetCreateAccount((create) => !create)
    }

    function login(username, password, url) {
        const fetchLog = async () => {
            try {
                const result = await fetch(url, {
                    method: 'POST',
                    body: JSON.stringify({
                        username,
                        password,
                    }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })
                const dataResult = await result.json()
                if (dataResult.token) {
                    navigate('/')
                    authContext.login(
                        dataResult.token,
                        dataResult.userId,
                        dataResult.userAdmin,
                    )
                } else {
                    setErrorAuth(true)
                }
            } catch (error) {
                console.log("Pas de réponse de l'API")
            }
        }
        fetchLog()
    }

    const onSubmit = (data, e) => {
        e.preventDefault()

        const { firstName } = data
        const { lastName } = data
        const { birthDate } = data
        const { email } = data
        const { username } = data
        const { password } = data
        const { verifyPassword } = data

        const urlSignup = `${process.env.REACT_APP_DB_CONNECT}/auth/signup`
        const urlLogin = `${process.env.REACT_APP_DB_CONNECT}/auth/Login`

        if (createAccount === true) {
            const fecthLog = async () => {
                try {
                    const resultSignup = await fetch(urlSignup, {
                        method: 'POST',
                        body: JSON.stringify({
                            firstName,
                            lastName,
                            birthDate,
                            email,
                            username,
                            password,
                            verifyPassword,
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })

                    const dataResultSignup = await resultSignup.json()
                    console.log(dataResultSignup)

                    login(username, password, urlLogin)
                } catch (error) {
                    console.log("Pas de réponse de l'API")
                }
            }
            fecthLog()
        } else {
            login(username, password, urlLogin)
        }
    }

    return (
        <div className="flex justify-center items-center pb-[100px]">
            <div className="auth_design h-auto m-auto w-[30rem] mt-[13rem] shadow p-5 shadow-slate-500/50 rounded-3xl">
                <h2 className="uppercase font-bold text-2xl">Identification</h2>
                <form onSubmit={handleSubmit(onSubmit)} className="w-full">
                    <div className="">
                        {createAccount && (
                            <div className="flex flex-col">
                                <label
                                    htmlFor="firstName"
                                    className="mr-8 mb-2 mt-8"
                                >
                                    Votre prénom
                                </label>
                                <input
                                    type="text"
                                    id="firstName"
                                    name="firstName"
                                    className="bg-slate-300 p-2 rounded-md"
                                    {...register('firstName', {
                                        required: 'Champ Ologatoire',
                                    })}
                                />
                            </div>
                        )}
                        {createAccount && (
                            <div className="mt-10 flex flex-col">
                                <label htmlFor="lastName" className="mr-8 mb-2">
                                    Votre nom
                                </label>
                                <input
                                    type="text"
                                    id="lastName"
                                    name="lastName"
                                    aria-label="lastName"
                                    className="bg-slate-300 p-2 rounded-md"
                                    {...register('lastName', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            </div>
                        )}
                        {createAccount && (
                            <div className="mt-10 flex flex-col">
                                <label htmlFor="email" className="mr-8 mb-2">
                                    Votre email
                                </label>
                                <input
                                    type="email"
                                    id="email"
                                    name="email"
                                    aria-label="email"
                                    className="bg-slate-300 p-2 rounded-md"
                                    {...register('email', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            </div>
                        )}
                        <div className="mt-10 flex flex-col">
                            <label
                                htmlFor="username"
                                className="mr-8 mb-2"
                                id="username"
                                label="username"
                            >
                                Votre pseudo
                            </label>
                            {errorAuth ? (
                                <input
                                    type="text"
                                    id="username"
                                    name="username"
                                    aria-label="username"
                                    aria-invalid="false"
                                    className="bg-slate-300 p-2 rounded-md border-4 border-red-600"
                                    {...register('username', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            ) : (
                                <input
                                    type="text"
                                    id="username"
                                    name="username"
                                    aria-label="username"
                                    aria-invalid="false"
                                    className="bg-slate-300 p-2 rounded-md"
                                    {...register('username', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            )}
                        </div>
                        {createAccount && (
                            <div className="mt-10 flex flex-col">
                                <label
                                    htmlFor="birthDate"
                                    className="mr-8 mb-2"
                                >
                                    Date de naissance
                                </label>
                                <input
                                    type="date"
                                    id="birthDate"
                                    name="birthDate"
                                    aria-label="birthDate"
                                    className="bg-slate-300 p-2 rounded-md"
                                    {...register('birthDate', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            </div>
                        )}
                    </div>

                    <div className="flex flex-col mb-2">
                        <div className="mt-10 flex flex-col">
                            <label htmlFor="password" className="mr-8 mb-2">
                                Votre mot de passe
                            </label>
                            {errorAuth ? (
                                <input
                                    type="password"
                                    id="password"
                                    aria-label="password"
                                    name="password"
                                    className="bg-slate-300 p-2 rounded-md border-4 border-red-600"
                                    {...register('password', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            ) : (
                                <input
                                    type="password"
                                    id="password"
                                    aria-label="password"
                                    name="password"
                                    className="bg-slate-300 p-2 rounded-md"
                                    {...register('password', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            )}
                            {errorAuth && (
                                <p className="text-red-700">
                                    Pseudo ou mot de passe incorect
                                </p>
                            )}
                        </div>
                        {createAccount && (
                            <div className="mt-10 flex flex-col mb-2">
                                <label
                                    htmlFor="verifyPassword"
                                    className="mr-8 mb-2"
                                >
                                    Vérification du mot de passe
                                </label>
                                <input
                                    type="password"
                                    id="verifyPassword"
                                    className="bg-slate-300 p-2 rounded-md"
                                    name="verifyPassword"
                                    {...register('verifyPassword', {
                                        required: 'Champ Obligatoire',
                                    })}
                                />
                            </div>
                        )}
                    </div>
                    <div>
                        <button
                            type="submit"
                            className="font-bold text-xl text-white bg-gray-500 rounded-md p-3 mt-10 w-1/2 m-auto text-center flex justify-center hover:bg-slate-500/80"
                        >
                            {createAccount
                                ? 'Créer son compte'
                                : 'Se connecter'}
                        </button>
                    </div>
                    <p
                        onClick={createAccountHandle}
                        className="hover:font-bold mt-5 hover:cursor-pointer text-center"
                    >
                        {createAccount
                            ? 'Allez vous connecté'
                            : "Si vous n'êtes pas enregistré : Créez votre compte"}
                    </p>
                </form>
            </div>
        </div>
    )
}

export default Auth
