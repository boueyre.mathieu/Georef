/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import { fireEvent, render } from '@testing-library/react'
import { describe, expect, jest } from '@jest/globals'
import { BrowserRouter as Router } from 'react-router-dom'
import Auth from './auth'

describe('Validate connexion form', () => {
    test('login form should be in the document', () => {
        const component = render(
            <Router>
                <Auth />
            </Router>,
        )
        const inputNode = component.getByRole('textbox', { name: /username/i })
        expect(inputNode).toBeInTheDocument()
    })

    test('username field have a label', () => {
        const component = render(
            <Router>
                <Auth />
            </Router>,
        )
        const usernameInputNode = component.getByRole('textbox', {
            name: /username/i,
        })
        expect(usernameInputNode.getAttribute('name')).toBe('username')
    })

    test('username field accept text', () => {
        const { getByLabelText } = render(
            <Router>
                <Auth />
            </Router>,
        )
        const usernameInputNode = getByLabelText('username')
        expect(usernameInputNode.value).toMatch('')
        fireEvent.change(usernameInputNode, { target: { value: 'testing' } })
        expect(usernameInputNode.value).toMatch('testing')
    })

    test('should be able to submit form', () => {
        const { getByRole } = render(
            <Router>
                <Auth />
            </Router>,
        )
        const buttonNode = getByRole('button')
        fireEvent.submit(buttonNode)
    })
})
