/* eslint-disable import/order */
/* eslint-disable linebreak-style */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable linebreak-style */
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './css/index.css'
import { BrowserRouter } from 'react-router-dom'
import { AuthContextProvider } from './context/authContext'
import ScrollToTop from './components/ScrollToTop/ScrollToTop'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
        <AuthContextProvider>
            <BrowserRouter>
                <ScrollToTop />
                <App />
            </BrowserRouter>
        </AuthContextProvider>
    </React.StrictMode>,
)
