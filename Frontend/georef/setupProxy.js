/* eslint-disable func-names */
/* eslint-disable import/no-extraneous-dependencies */
const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function (app) {
    app.use(
        '/api',
        createProxyMiddleware({
            // 👇️ make sure to update your target
            target: 'http://localhost:4000/',
            changeOrigin: true,
        }),
    )
}
