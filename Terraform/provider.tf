provider "azurerm" {
  skip_provider_registration="true"
  features {}
}
provider "azapi" {
}

terraform {
  backend "azurerm" {}
  required_providers {
     azurerm= {
      source="hashicorp/azurerm"
      version = "3.74.0"
    }
    azapi = {
      source = "Azure/azapi"
    }
  }
}
