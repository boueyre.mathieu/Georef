resource "azurerm_resource_group" "rg" {
  name     = "geo3ref"
  location = "West Europe"
}

resource "azurerm_virtual_network" "georefvnet" {
  name                = "georef2-network"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.0.0.0/16"]

  tags = {
    environment = "Production"
  }
}

resource "azurerm_subnet" "geo_flexible_subnet" {
  name                 = "georef2-flexible-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.georefvnet.name
  address_prefixes     = ["10.0.2.0/24"]
  service_endpoints    = ["Microsoft.Storage"]
  delegation {
    name = "fs"
    service_delegation {
      name = "Microsoft.DBforMySQL/flexibleServers"
      actions = [
        "Microsoft.Network/virtualNetworks/subnets/join/action",
      ]
    }
  }
}

resource "azurerm_subnet" "geo_back_subnet" {
  name                 = "geo2-back-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.georefvnet.name
  address_prefixes     = ["10.0.1.0/24"]

  delegation {
    name = "Farm-delegation"

    service_delegation {
      name    = "Microsoft.Web/serverFarms"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}


resource "azurerm_mysql_flexible_server" "sqlserver" {
  name                   = "georef2-fs"
  resource_group_name    = azurerm_resource_group.rg.name
  location               = azurerm_resource_group.rg.location
  administrator_login    = "Bauer"
  administrator_password = "Pigloo1234&"
  backup_retention_days  = 7
  delegated_subnet_id    = azurerm_subnet.geo_flexible_subnet.id
  sku_name               = "GP_Standard_D2ds_v4"
  zone                   = 3

}


resource "azurerm_mysql_flexible_database" "geodb" {
  name                = "georef2"
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_mysql_flexible_server.sqlserver.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

# resource "azurerm_mysql_virtual_network_rule" "geovnetrule" {
#   name                  = "geo2-vnet-rule"
#   resource_group_name   = azurerm_resource_group.rg.name
#   server_name           = azurerm_mysql_flexible_server.sqlserver.name
#   subnet_id             = azurerm_subnet.geosubnet.id
# }


resource "azurerm_static_site" "frontend" {
  name                = "frontend-georef2-static-site"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku_tier            = "Standard"
  sku_size            = "Standard"
}


resource "azurerm_service_plan" "backend" {
  name                = "backend-georef2-plan"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku_name            = "B1"
  os_type             = "Linux"
}


resource "azurerm_linux_web_app" "backend" {
  name                      = "backend-georef2-appservice"
  location                  = azurerm_resource_group.rg.location
  resource_group_name       = azurerm_resource_group.rg.name
  service_plan_id           = azurerm_service_plan.backend.id
  virtual_network_subnet_id = azurerm_subnet.geo_back_subnet.id

  site_config {
    always_on              = true
    local_mysql_enabled    = true
    vnet_route_all_enabled = true
  }

  app_settings = {
    "WEBSITES_PORT" = "4000"
  }
}

resource "azapi_resource" "geoapilink" {
  type       = "Microsoft.Web/staticSites/linkedBackends@2022-09-01"
  name       = "geoapilink2"
  parent_id  = azurerm_static_site.frontend.id
  body = jsonencode({
    properties = {
      backendResourceId = azurerm_linux_web_app.backend.id
      region            = azurerm_resource_group.rg.location
    }
  })
}


output "frontend_token" {
  description = "Token for static site Frontend"
  value       = azurerm_static_site.frontend.api_key
  sensitive   = true
}

output "frontend_URL" {
  description = "URL for static site Frontend"
  value       = azurerm_static_site.frontend.default_host_name
  sensitive   = true
}



