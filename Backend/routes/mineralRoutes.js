const express = require('express')
const router = express.Router()

const mineralCtrl = require('../controllers/mineral')
const multer= require('../middleware/multer-config')
const auth = require('../middleware/auth')

router.post('/createMineral', auth, multer, mineralCtrl.createMineral)
router.get('/getAllCrystalMesh', mineralCtrl.getAllCrystalMesh)
router.get('/getAllFamily', mineralCtrl.getAllFamily)
router.get('/getAllHabitus', mineralCtrl.getAllHabitus)
router.get('/family/:family', mineralCtrl.getAllMineralToDisplayInOneFamily)
router.post('/updateMineral/:id',auth, multer, mineralCtrl.updateMineral)
router.get('/displayOneMineral/:id', mineralCtrl.displayOneMineral)
router.get('/displayOneCrystalMesh/:id', mineralCtrl.getOneCrystalMesh)
router.get('/displayOneFamily/:id', mineralCtrl.getOneFamily)
router.get('/displayOneHabitus/:id', mineralCtrl.getOneHabitus)
router.get('/getAllMineralsToDisplay/:name', mineralCtrl.getAllMineralsToDisplay)
router.get('/getAllMineralsToDisplay/', mineralCtrl.getAllMineralsToDisplay)
router.get('/userMinerals', auth, mineralCtrl.getUserMinerals)
router.get('/userMineralId/:id', auth, mineralCtrl.displayOneMineral)
router.delete('/delete/:id', auth, mineralCtrl.userDelete)



module.exports = router