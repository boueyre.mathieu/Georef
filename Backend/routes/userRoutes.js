const express = require('express')
const router = express.Router()

const userCrtl = require('../controllers/user')
const auth = require('../middleware/auth')

router.post('/signup', userCrtl.signup)
router.post('/login', userCrtl.login)
router.put('/changePwd', auth,  userCrtl.changePwd)
router.get('/user', auth, userCrtl.getOneUser)


module.exports = router