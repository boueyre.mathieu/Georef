const express = require('express')
const router = express.Router()

const mineralCtrl = require('../controllers/mineral')
const auth = require('../middleware/auth')

router.get('/', auth)
router.get('/getAllMineralsNotChecked', auth, mineralCtrl.getAllMineralsNotChecked)
router.put('/check/:id', auth,  mineralCtrl.adminCheck)
router.put('/display/:id', auth,  mineralCtrl.adminDisplay)
router.delete('/delete/:id', auth, mineralCtrl.adminDelete)



module.exports = router