/* eslint-disable linebreak-style */
/* eslint-disable no-undef */
const express = require('express')
const app = express()
const path = require('path')
// const rateLimit = require('express-rate-limit')
const helmet = require('helmet')
const cors = require('cors')


app.use(helmet());
// app.use(helmet({ crossOriginResourcePolicy: { policy: "same-site" } }));
// app.use(helmet({ crossOriginResourcePolicy: { policy: "same-origin" } }));
// app.use(helmet({ crossOriginResourcePolicy: false }));


const userRoutes = require('./routes/userRoutes')
const mineralRoutes = require('./routes/mineralRoutes')
const adminRoutes = require('./routes/adminRoutes')

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Credentials', true);
	res.setHeader("Cross-Origin-Resource-Policy", "cross-origin");
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader(
		'Access-Control-Allow-Headers',
		'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'
	)
	res.setHeader(
		'Access-Control-Allow-Methods',
		'GET, POST, PUT, DELETE, PATCH, OPTIONS'
	)
	next()
})

// const apiLimiter = rateLimit({
// 	windowMs: 15 * 60 * 1000, // 15 minutes
// 	max: 100, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
// 	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
// 	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
// })

// app.use('/api/auth', apiLimiter)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/api/auth', userRoutes)
app.use('/api/mineral', mineralRoutes)
app.use('/api/admin', adminRoutes)

app.use('/images', express.static(path.join(__dirname, 'images')))

module.exports = app
