const {Sequelize, DataTypes } = require('sequelize')
const sequelize = require('../DB/dbconnection')

async function initUser() {
    
    sequelize.transaction(async (transaction) => {
      try {
        
        const habitusCount = await User.count()
    
        if(habitusCount > 0)
          return
    
        const sql = `
          INSERT INTO user (id, firstName, lastName, birthDate, email, username, password, createdAt, isAdmin) VALUES
            (1, 'Grand', 'ADMINISTRATOR', '1998-05-21 00:00:00', 'grand.administrator@exemple.com', 'GA', '$2b$10$/rQ28VI1EiK4ZRIPubFyBeRzRWCXjRg/x/b2yUIXpYWiaXo7UyayS', '2023-10-02 07:52:35', 1);
        `;
    
        await sequelize.query(sql, { transaction });
        console.log('Data have been inserted');
  
      } catch (error) {
        // Rollback the transaction if an error occurs
        console.log('Error occurred:', error);
        throw error
      }
    });
  }


const User = sequelize.define('user', {
      id:{type: DataTypes.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      firstName:{type: DataTypes.STRING, allowNull: false},
      lastName:{type: DataTypes.STRING, allowNull: false},
      birthDate:{type: DataTypes.DATE, allowNull: false},
      email:{type: DataTypes.STRING, allowNull: false, unique: true},
      username:{type: DataTypes.STRING, allowNull: false, unique: true},
      password:{type: DataTypes.STRING, allowNull: false},
      createdAt:{type: DataTypes.DATE, allowNull: false,  defaultValue: DataTypes.NOW},
      isAdmin:{type: DataTypes.TINYINT, allowNull: false, defaultValue: 0},
  },
  {tableName: 'user', timestamps: false},
);

(async () => {
  try {
    await User.sync();
    console.log('USer table created');

    await initUser();
  } catch (error) {
    console.log('Error occurred:', error);
  }
})();






exports.default = User
