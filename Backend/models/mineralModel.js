const {Sequelize, DataTypes, HasOne} = require('sequelize')
const sequelize = require('../DB/dbconnection')

const Mineral = sequelize.define('mineral', {
      id:{type: DataTypes.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      name:{type: DataTypes.STRING, allowNull: false},
      chemicalFormula:{type: DataTypes.STRING, allowNull: false},
      hardness:{type: DataTypes.DECIMAL(4,2), allowNull: false},
      glow:{type: DataTypes.STRING, allowNull: false},
      color:{type: DataTypes.STRING, allowNull: false},
      opacity:{type: DataTypes.STRING, allowNull: false},
      density:{type: DataTypes.DECIMAL(4,2), allowNull: false},
      picturePath:{type: DataTypes.TEXT, allowNull: false},
      description:{type: DataTypes.TEXT, allowNull: false},
      createdAt:{type: DataTypes.DATE, allowNull: false,  defaultValue: DataTypes.NOW},
      family_id:{type: DataTypes.INTEGER, allowNull: false},
      habitus_id:{type: DataTypes.INTEGER, allowNull: false},
      crystalMesh_id:{type: DataTypes.INTEGER, allowNull: false},
      user_id:{type: DataTypes.INTEGER, allowNull: false},
      isDisplay:{type: DataTypes.BOOLEAN, allowNull: false,  defaultValue: 0},
      isChecked:{type: DataTypes.BOOLEAN, allowNull: false,  defaultValue: 0}
  },
  {tableName: 'mineral', timestamps: false},
)





exports.default = Mineral
