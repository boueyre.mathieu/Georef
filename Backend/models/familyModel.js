const {Sequelize, DataTypes, HasMany, HasOne } = require('sequelize')
const sequelize = require('../DB/dbconnection')


async function initFamilies() {
    
    sequelize.transaction(async (transaction) => {
      try {
        
        const familyCount = await Family.count()
    
        if(familyCount > 0)
          return
    
        const sql = `
          INSERT INTO family (id, label, description, familyPicture) VALUES
            (1, 'Éléments Natifs', NULL, 'https://www.france-mineraux.fr/wp-content/uploads/2018/11/pierre-cuivre.jpg'),
            (2, 'Sulfures et sulfosels', NULL, 'https://thumbs.dreamstime.com/b/quartz-avec-des-sulfures-sur-un-fond-blanc-69649063.jpg'),
            (3, 'Halogénures et oxydes', NULL, 'https://www.madagascandirect.com/uploads/products/rainbow-fluorite-8u.jpg'),
            (4, 'Carbonates et borates', NULL, 'https://cdn.webshopapp.com/shops/281026/files/360444246/650x750x2/azurite-malachite-brute.jpg'),
            (5, 'Sulfates', NULL, 'https://lamine2tout.com/14500-large_default/calcedoine-bleue-environs-de-rodez-aveyron-france.jpg'),
            (6, 'Phosphates', NULL, 'https://media.istockphoto.com/id/666934596/fr/photo/phosphorite-de-pierre-min%C3%A9rale-ou-phosphate-de-roche-isol%C3%A9-sur-fond-blanc-la-phosphorite.jpg?s=170667a&w=0&k=20&c=B5hwURZ3hcH79U-PlznI-Bz55vgLyNhZ1YJrFX5lSY4='),
            (7, 'Silicates et composé organiques', NULL, 'https://www.bracelet-chakra-blog.fr/wp-content/uploads/2019/09/pierre-grenat-pyrope.jpg');
        `;
    
        await sequelize.query(sql, { transaction });
        console.log('Data have been inserted');
  
      } catch (error) {
        // Rollback the transaction if an error occurs
        console.log('Error occurred:', error);
        throw error
      }
    });
  }

const Family = sequelize.define(
  'family',
  {
    id: { type: DataTypes.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true },
    label: { type: DataTypes.STRING, allowNull: false },
    description: { type: DataTypes.STRING, allowNull: true },
    familyPicture: { type: DataTypes.TEXT, allowNull: false },
  },
  { tableName: 'family', timestamps: false }
);

(async () => {
  try {
    await Family.sync();
    console.log('Family table created');

    await initFamilies();
  } catch (error) {
    console.log('Error occurred:', error);
  }
})();

exports.default = Family