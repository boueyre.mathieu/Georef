const {Sequelize, DataTypes } = require('sequelize')
const sequelize = require('../DB/dbconnection')

async function initCrystalMesh() {
    
    sequelize.transaction(async (transaction) => {
      try {
    
        const CrystalMeshinit = await CrystalMesh.count()
    
        if(CrystalMeshinit > 0)
          return
    
        const sql = `
          INSERT INTO crystalmesh (id, label, description) VALUES
            (1, 'Cubique', NULL),
            (2, 'Hexagonal', NULL),
            (3, 'Rhomboédrique', NULL),
            (4, 'Tetragonal', NULL),
            (5, 'Orthorombique', NULL),
            (6, 'Monoclinique', NULL),
            (7, 'Triclinique', NULL);
          `;
    
        await sequelize.query(sql, { transaction });
        console.log('Data have been inserted');
  
      } catch (error) {
        // Rollback the transaction if an error occurs
        console.log('Error occurred:', error);
        throw error
      }
    });
  }


const CrystalMesh = sequelize.define('crystalMesh', {
      id:{type: DataTypes.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      label:{type: DataTypes.STRING, allowNull: false},
      description:{type: DataTypes.STRING, allowNull: true},
  },
  {tableName: 'crystalMesh', timestamps: false},
);

(async () => {
  try {
    await CrystalMesh.sync();
    console.log('Family table created');

    await initCrystalMesh();
  } catch (error) {
    console.log('Error occurred:', error);
  }
})();

exports.default = CrystalMesh