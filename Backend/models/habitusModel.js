const {Sequelize, DataTypes } = require('sequelize')
const sequelize = require('../DB/dbconnection')

async function initHabitus() {
    
    sequelize.transaction(async (transaction) => {
      try {
        
        const habitusCount = await Habitus.count()
    
        if(habitusCount > 0)
          return
    
        const sql = `
          INSERT INTO habitus (id, label) VALUES
            (1, 'Ariculaire'),
            (2, 'Foliacé'),
            (3, 'Botryoïde'),
            (4, 'Dendritique'),
            (5, 'Fibreux'),
            (6, 'Lamellaire'),
            (7, 'Mamelonné'),
            (8, 'Massif'),
            (9, 'Rayonnant'),
            (10, 'Réniforme'),
            (11, 'Tabulaire'),
            (12, 'Bacillaire');
        `;
    
        await sequelize.query(sql, { transaction });
        console.log('Data have been inserted');
  
      } catch (error) {
        // Rollback the transaction if an error occurs
        console.log('Error occurred:', error);
        throw error
      }
    });
  }
const Habitus = sequelize.define('habitus', {
      id:{type: DataTypes.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      label:{type: DataTypes.STRING, allowNull: false},
  },
  {tableName: 'habitus', timestamps: false},
);

(async () => {
  try {
    await Habitus.sync();
    console.log('Habitus table created');

    await initHabitus();
  } catch (error) {
    console.log('Error occurred:', error);
  }
})();


exports.default = Habitus