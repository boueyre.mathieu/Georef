const {Sequelize, DataTypes,Op, QueryTypes } = require('sequelize')
const { default: Mineral} = require('../models/mineralModel')
const { default: Family} = require('../models/familyModel')
const { default: CrystalMesh} = require('../models/crystalMeshModel')
const { default: Habitus} = require('../models/habitusModel')
const sequelize = require('../DB/dbconnection')
const fs = require('fs')

exports.createMineral = async(req,res) => {
    const picture = `${req.protocol}://${req.get('host')}/images/${
        req.file.filename
    }`
    
    const mineral = await Mineral.findOne({where: {
        [Op.or]:[{name:req.body.name}, {chemicalFormula:req.body.chemicalFormula}] }})
    
    if (mineral){
        res.status(400).json({ message: 'Le minéral existe déjà'})
        return
    }
    else {
        const mineral = Mineral.create({
            name: req.body.name,
            chemicalFormula: req.body.chemicalFormula,
            hardness: req.body.hardness,
            glow: req.body.glow,
            color: req.body.color,
            opacity: req.body.opacity,
            density: req.body.density,
            picturePath: picture,
            description: req.body.description,
            family_id: req.body.family,
            habitus_id: req.body.habitus,
            crystalMesh_id: req.body.crystalMesh,
            user_id:req.auth.userId
        })
        if(!mineral){
            res.status(400).json({message:'N existe pas'})
        }else{
            res.status(200).json({message:'Minéral enregistré'})
        }
    }
    
}

exports.getAllCrystalMesh = async (req, res) => {
    const getAllCrystalMesh = await CrystalMesh.findAll()
    if(!getAllCrystalMesh){
        res.status(400).json({message:'Aucune maille cristalline trouvée'})
        return
    }else{
        res.status(200).json(getAllCrystalMesh)
    }
}

exports.getOneCrystalMesh = async (req, res) => {
    const getOneCrystalMesh = await CrystalMesh.findOne({where:{id:req.params.id}})
    if(!getOneCrystalMesh){
        res.status(400).json({message:'Aucune maille cristalline trouvée'})
        return
    }else{
        res.status(200).json(getOneCrystalMesh)
    }
}

exports.getAllFamily = async (req, res) => {
    const getAllFamily = await Family.findAll()
    if(!getAllFamily){
        res.status(400).json({message:'Aucune famille cristalline trouvée'})
        return
    }else{
        res.status(200).json(getAllFamily)
    }
}

exports.getOneFamily = async (req, res) => {
    const getOneFamily = await Family.findOne({where:{id:req.params.id}})
    if(!getOneFamily){
        res.status(400).json({message:'Aucune maille cristalline trouvée'})
        return
    }else{
        res.status(200).json(getOneFamily)
    }
}

exports.getOneHabitus = async (req, res) => {
    const getOneHabitus = await Habitus.findOne({where:{id:req.params.id}})
    if(!getOneHabitus){
        res.status(400).json({message:'Aucune maille cristalline trouvée'})
        return
    }else{
        res.status(200).json(getOneHabitus)
    }
}

exports.getAllHabitus = async (req, res) => {
    const getAllHabitus = await Habitus.findAll()
    if(!getAllHabitus){
        res.status(400).json({message:'Aucune famille cristalline trouvée'})
        return
    }else{
        res.status(200).json(getAllHabitus)
    }
}
exports.getAllMineralToDisplayInOneFamily = async(req, res) => {
    console.log(req.params.family)
    const family = await Family.findOne({where:{label: req.params.family}})
    const getMineralInFamily = await Mineral.findAll({
        where: {[Op.and]:[{isDisplay: 1},{isChecked:1},{family_id:family.id}]},
    })
    if(!getMineralInFamily){
        res.status(400).json({message:'Pas de minéral'})
    }else{
        res.status(200).json(getMineralInFamily)
    }
}


exports.getAllMineralsNotChecked = async (req, res) => {
    const getAllMineralsNotDisplayed = await Mineral.findAll({where: {
        [Op.and]:[{isDisplay: 0},{isChecked:0}]}})
    if(!getAllMineralsNotDisplayed){
        res.status(400).json({message:'Aucun mineral non checké'})
        return
    }
    else{
        res.status(200).json(getAllMineralsNotDisplayed)
    }
    
}

exports.getAllMineralsToDisplay = async (req, res) => {
    let allMineralsToDisplay
    if(!req.params.name){
        allMineralsToDisplay = await Mineral.findAll({where: {
            [Op.and]:[{isDisplay: 1},{isChecked:1}]}})
    }
    else {
        allMineralsToDisplay = await Mineral.findAll({where: {
            [Op.and]:[{name:{
                [Op.like]: `%${req.params.name}%`}
                },{isDisplay: 1},{isChecked:1}]}})
    }
    if(!allMineralsToDisplay){
        res.status(400).json({message:'Aucun mineral trouvé'})
        return
    }
    else{
        res.status(200).json(allMineralsToDisplay)
    }
}

exports.getUserMinerals = async(req, res) => {
    const userMinerals = await Mineral.findAll({where:{user_id: req.auth.userId}})
    if(!userMinerals){
        res.status(400).json({message:'Ne possède aucun minéral'})
        return
    }else{
        res.status(200).json(userMinerals)
    }
}


exports.updateMineral = async(req,res) => {   
    
    const mineral = await Mineral.findOne({where:{
        [Op.and]:[{id:req.params.id},{user_id:req.auth.userId}]}})

    if (mineral){
        mineral.set({
            name: req.body.name,
            chemicalFormula: req.body.chemicalFormula,
            hardness: req.body.hardness,
            glow: req.body.glow,
            color: req.body.color,
            opacity: req.body.opacity,
            density: req.body.density,
            description: req.body.description,
            family_id: req.body.family,
            habitus_id: req.body.habitus,
            crystalMesh_id: req.body.crystalMesh,
            isChecked: false,
            isDisplay: false
        })

        if(!req.file){
            mineral.set({
                picturePath: req.body.image
            })
            
        }else{
            const filename = mineral.picturePath.split('/images/')[1]
            fs.unlinkSync(`images/${filename}`)
            const picture = `${req.protocol}://${req.get('host')}/images/${
            req.file.filename
            }`
            mineral.set({
                picturePath: picture
            })
        }

    
        console.log('par ici aussi avec ' + mineral)
        console.log(req.protocol)
        await mineral.save()
        res.status(200).json({mesasge:'Élément enregistré'})

    }
    else {
        res.status(400).json({message:'N existe pas'})
        return
    }
}

exports.adminCheck = async(req,res) => {
    const mineral = await Mineral.findOne({where:{id:req.params.id}})

    if(!mineral){
        res.status(400).json({message:'N existe pas'})
        return
    }
    else{
        mineral.isChecked = true
        await mineral.save()
        res.status(200).json({message:'Minréral checked mais non validé'})
    }
}

exports.adminDisplay = async(req,res) => {
    const mineral = await Mineral.findOne({where:{id:req.params.id}})

    if(!mineral){
        res.status(400).json({message:'N existe pas'})
        return
    }
    else{
        mineral.isChecked = true
        mineral.isDisplay = true
        await mineral.save()
        res.status(200).json({message:'Minréral checked et validé'})
    }
}

exports.adminDelete = async(req,res) => {
    const mineral = await Mineral.findOne({where:{id:req.params.id}})

    if(!mineral){
        res.status(400).json({message:'N existe pas'})
        return
    }
    else{
        await mineral.destroy()
        res.status(200).json({message:'Minéral supprimé'})
    }
}

exports.userDelete = async(req,res) => {
    const mineral = await Mineral.findOne({where:{id:req.params.id}})

    if(!mineral){
        res.status(400).json({message:'N existe pas'})
        return
    }
    else{
        await mineral.destroy()
        res.status(200).json({message:'Minéral supprimé'})
    }
}

exports.displayOneMineral = async(req,res) => {
    const mineral = await Mineral.findOne({where:{id:req.params.id}})
    if(!mineral){
        res.status(400).json({message:'N existe pas'})
        return
    }
    else{
        res.status(200).json(mineral)
    }
}