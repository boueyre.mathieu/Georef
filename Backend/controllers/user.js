const bcrypt = require('bcrypt')
const jxt = require('jsonwebtoken')
const {Sequelize, DataTypes } = require('sequelize')
const { default: User} = require('../models/userModel')


exports.signup = async (req, res) =>  {
  const {username, verifyPassword, email, password} = req.body

  if (password !== verifyPassword){
    res.status(403).send({
      message: "La vérification du mdp n'est pas bonne"
    })
    return
  } 
  const user = await User.findOne({where: {username: username}})
  console.log(user)
    if(user){
      res.status(400).json({ message: 'Le pseudo existe' })
      return
    }
  const findUser = await User.findOne({where: {email: req.body.email}})
  if(findUser){
    res.status(400).json({ message:  'L\'email existe' })
    return
  }else{
    bcrypt
    .hash(req.body.password, 10)
    .then(hash => {
      const user = User.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        birthDate: req.body.birthDate,
        email: req.body.email,
        username: req.body.username,
        password: hash
      })
      .then(result => {
        res.status(200).json({ message : 'Utilisateur enregistré' })
      })
      .catch(error => res.status(500).json({ message: 'Erreur de la base de donnée', error })) 
    })
  }
}

function comparePassword(plainPasswordToCompare, passwordKnown){
  return bcrypt.compareSync(plainPasswordToCompare, passwordKnown)
} 
        
        
exports.login = async (req, res) => {
  const {username, password} = req.body
  console.log(password)
  const user = await User.findOne({where:{username: username}})
  if(user === null){
    console.log(user)
    res.status(400).json({ message : 'Paire pseudo/mdp incorrecte' })
  }else{
    const pwdMatch = comparePassword(password, user.password)
    if(pwdMatch === false){
      res.status(400).json({ message: 'Paire login/mdp incorrecte' })
      return 
    }else{
      res.status(200).json({
        username : user.dataValues.username,
        userId : user.dataValues.id,
        userAdmin : user.dataValues.isAdmin,
        token : jxt.sign(
          { userId: user.dataValues.id, userAdmin: user.dataValues.isAdmin },
          'RANDOM_TOKEN_SECRET',
          {
            expiresIn: '24h',
          }
        ),
        message: 'utilisateur connecté',
      })
    }
  }
}

exports.changePwd = async (req, res) => {
  const {oldPassword, newPassword, verifyNewPassword} = req.body
  const id = req.auth.userId

  const user = await User.findOne({where:{id: id}})
  if(!user){
    res.status(403).json({ message: 'Utilisateur non trouvé' })
    return
  }else{
    const matchPassword = comparePassword(oldPassword, user.password)
    if(!matchPassword){
      res.status(400).json({ message: 'Mot de passe incorecte' })
      return
    }else{
      if(verifyNewPassword !== newPassword){
        res.status(400).json({ message: 'La vérification du mot de passe est incorecte' })
      }else{
        bcrypt
          .hash(newPassword, 10)
          .then((hash) => {
            User.update(
              {password: hash},
              {where: {id: user.dataValues.id}}
            )
            .then((result) => {
              res.status(200).json({ message: 'mdp modifié' })
            })
            .catch((err) => {
              res.status(400).json({ messsage : 'Impossible de modifié le mdp' })
            })
        })
      }
    }
  }
}


exports.getOneUser = async (req, res) => {
  const id = req.auth.userId
  const getUser = await User.findOne({where: {id: id}})
  if(!getUser){
    res.status(400).json({message:'utilisateur non trouvé'})
    return
  }else{
    res.status(200).json(getUser)
  }
}
