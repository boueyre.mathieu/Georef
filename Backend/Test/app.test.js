const {Sequelize, Datatype} = require('sequelize')
const app = require('../app')
const request = require('supertest')
const dotenv = require('dotenv')
const configDotenv = dotenv.config()

const DATABASE_NAME = "georef"
const DATABASE_USER = "root"
const DATABASE_PASSWORD = "password"
const DB_ADRESS = 'localhost'

const sequelize = 
    new Sequelize(DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD, {
        host:DB_ADRESS,
        dialect:'mysql',
        logging:console.log
    })

describe('Mineral Route', () => {
  beforeAll(async() => {
    try {
      await sequelize.authenticate();
      console.log('Connection to the database has been established successfully.');
      await sequelize.sync();
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }
  })

  afterAll(async () => {
    await sequelize.close();
  })

  it('should return status code 200', async () => {
    const response = await request(app).get('/api/mineral/getAllCrystalMesh');
    expect(response.status).toBe(200);
  })

  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/getAllFamily')
    expect(response.status).toBe(200)
  })

  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/getAllHabitus')
    expect(response.status).toBe(200)
  })

  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/displayOneMineral/1')
  expect(response.status).toBe(200)
  })

  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/displayOneCrystalMesh/1')
  expect(response.status).toBe(200)
  })

  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/displayOneFamily/1')
  expect(response.status).toBe(200)
  })

  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/displayOneHabitus/1')
  expect(response.status).toBe(200)
  })
  
  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/getAllMineralsToDisplay/quartz')
  expect(response.status).toBe(200)
  })

  it('should return status code 200', async ()  => {
    const response = await request(app).get('/api/mineral/getAllMineralsToDisplay/')
  expect(response.status).toBe(200)
  })

  it('should return status code 401 because unauthorized', async ()  => {
    const response = await request(app).get('/api/mineral/userMinerals')
  expect(response.status).toBe(401)
  })

  it('should return status code 401 because unauthorized', async ()  => {
    const response = await request(app).get('/api/mineral/userMineralId/1')
  expect(response.status).toBe(401)
  })

  it('should return status code 401 because unauthorized', async ()  => {
    const response = await request(app).delete('/api/mineral/delete/1')
  expect(response.status).toBe(401)
  })

  it('should return status code 404', async ()  => {
    const response = await request(app).get('/')
  expect(response.status).toBe(404)
  })

  it('should return status code 404', async ()  => {
    const response = await request(app).get('/getAllMineralsNotChecked')
  expect(response.status).toBe(404)
  })
})




